<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryPost;

class Post extends Model
{
    protected $table = 'posts';

    public function category()
    {
        return $this->belongsTo(CategoryPost::class, 'category_id', 'id');
    }
    public static function getPostMostView($num)
    {
        $post = self::where('status', 1)->orderByDesc('view')->limit($num)->get();
        return $post;
    }
}
