<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\User;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if ($request->email == $user->email) {
            if ($request->dienthoai == $user->dienthoai) {
                return [
                    'ten' => 'required',
                    'email' => 'required',
                    'dienthoai' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
                ];
            } else {
                return [
                    'ten' => 'required',
                    'email' => 'required',
                    'dienthoai' => 'required|unique:users|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
                ];
            }
        } else {
            if ($request->dienthoai == $user->dienthoai) {
                return [
                    'ten' => 'required',
                    'email' => 'required|unique:users',
                    'dienthoai' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
                ];
            } else {
                return [
                    'ten' => 'required',
                    'email' => 'required|unique:users',
                    'dienthoai' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
                ];
            }
        }
    }
    public function messages()
    {
        return [
            'ten.required' => 'Tên không được để trống',
            'email.required' => 'Tài khoản không được để trống',
            'email.unique' => 'Tài khoản đã được sử dụng',
            'dienthoai.unique' => 'Số điện thoại đã được sử dụng',
            'dienthoai.required' => 'Số điện thoại không được để trống',
            'dienthoai.regex' => 'Số điện thoại không đúng định dạng'
        ];
    }
}