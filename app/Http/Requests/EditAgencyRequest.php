<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditAgencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required',
            'album' => 'required'

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'image.required' => 'Ảnh không được để trống',
            'album.required' => 'Album ảnh không được để trống'
        ];
    }
}