<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAccessoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'price' => 'required'

        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên phụ kiện không được để trống',
            'code.required' => 'Mã phụ kiện không được để trống',
            'price.required' => 'Giá phụ kiện không được để trống'
        ];
    }
}