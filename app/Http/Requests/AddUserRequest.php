<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ten' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:3|max:32',
            'dienthoai' => 'required|unique:users|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
        ];
    }
    public function messages()
    {
        return [
            'ten.required' => 'Tên không được để trống',
            'email.required' => 'Tài khoản không được để trống',
            'email.unique' => 'Tài khoản đã được sử dụng',
            'password.required' => 'Mật khẩu không được để trống',
            'password.min' => 'Mật khẩu không được ít hơn 4 ký tự',
            'password.max' => 'Mật khẩu không được nhiều hơn 31 ký tự',
            'dienthoai.unique' => 'Số điện thoại đã được sử dụng',
            'dienthoai.required' => 'Số điện thoại không được để trống',
            'dienthoai.regex' => 'Số điện thoại không đúng định dạng'
        ];
    }
}
