<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
            'address' => 'required',
            'title' => 'required',
            'description' => 'required',
            'keyword' => 'required',
            'image' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'email.required' => 'Email không được để trống',
            'phone.required' => 'Số điện thoại không được để trống',
            'phone.regex' => 'Số điện thoại không đúng định dạng',
            'address.required' => 'Địa chỉ không được để trống',
            'title.required' => 'Title không được để trống',
            'description.required' => 'Description không được để trống',
            'keyword.required' => 'Keyword không được để trống',
            'image.required' => 'Logo không được để trống'
        ];
    }
}