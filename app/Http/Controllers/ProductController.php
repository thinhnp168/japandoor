<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function allProduct()
    {
        $allProduct = Product::where('status', 1)->orderByDesc('arrange')->paginate(15);
        $listCategory = Category::where('status', 1)->get();
        return view('client.product.all', compact('allProduct', 'listCategory'));
    }
    public function listProduct($slug, $id)
    {
        $listCategory = Category::where('status', 1)->get();
        $category = Category::find($id);
        $listProduct = Product::where('status', 1)->where('category_id', $id)->orderByDesc('arrange')->paginate(15);
        return view('client.product.list', compact('listProduct', 'category', 'listCategory'));
    }
    public function detailProduct($slug, $id)
    {
        $product = Product::find($id);
        return view('client.product.detail', compact('product'));
    }
}