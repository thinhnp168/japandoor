<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Color;
use App\Accessory;
use App\Agency;
use App\Category;
use App\Contact;
use App\Email;
use App\Order;
use App\Product;
use App\Province;
use App\Slider;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function Login()
    {
        return view('admin.login');
    }
    public function PostLogin(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Kiểm tra đúng email và mật khẩu sẽ chuyển trang
            return redirect()->route('admin_home')->with('success', 'Đăng nhập thành công');
        } else {
            return redirect()->back()->with('error', 'Sai tài khoản hoặc mật khẩu');
        }
    }
    public function Logout()
    {
        Auth::logout();
        return redirect()->route('login')->with('success', 'Đăng xuất thành công');
    }
    public function home()
    {
        $post_typical = Post::where('category_id', 4)->get();
        $post_new = Post::where('category_id', 1)->get();
        $slider = Slider::where('status', 1)->orderByDesc('arrange')->get();
        $category = Category::where('status', 1)->where('highlight', 1)->orderByDesc('arrange')->get();
        $category_caculator = Category::where('status', 1)->get();
        $color_caculator = Color::where('status', 1)->get();
        $accessory_caculator = Accessory::where('status', 1)->get();
        return view('client.home.home', compact('slider', 'category', 'category_caculator', 'color_caculator', 'accessory_caculator', 'post_typical', 'post_new'));
    }
    public function contact()
    {
        return view('client.contact.contact');
    }
    public function saveContact(Request $request)
    {
        $contact = new Contact;
        $contact->title = $request->tieude;
        $contact->name = $request->hoten;
        $contact->phone = $request->sodienthoai;
        $contact->email = $request->email;
        $contact->address = $request->diachi;
        $contact->content = $request->thongtinkhac;
        $contact->save();
        if ($contact->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }

    public function saveEmail(Request $request)
    {
        $saveEmail = new Email;
        $saveEmail->email = $request->email;
        $saveEmail->save();
        if ($saveEmail->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }

    public function formOrder()
    {
        return view('client.order.order');
    }

    public function order(Request $request)
    {
        $order = new Order;
        $order->name = $request->name;
        $order->phone = $request->phone;
        $order->month = $request->month;
        $order->door = $request->door;
        $order->window = $request->window;
        $order->province = $request->province;
        $order->type = $request->type;
        $order->content = $request->content;
        $order->save();
        if ($order->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function searchProduct(Request $request)
    {
        $idCategory = $request->id_loaicuachon;
        $idColor = $request->id_maucua;
        $product = Product::where('category_id', $idCategory)->where('color_id', $idColor)->get();
        if (sizeof($product) == 0) {
            return response()->json([
                'type' => 2,
                'mess' => 'Chưa có hình ảnh sản phẩm phù hợp!'
            ]);
        } elseif (sizeof($product) == 1) {
            return response()->json([
                'type' => 1,
                'album' => json_decode($product->album)
            ]);
        } else {
            $album = [];
            foreach ($product as $key => $value) {
                foreach (json_decode($value->album) as $key1 => $value1) {
                    $album[] = $value1;
                }
            }
            return response()->json([
                'type' => 1,
                'album' => $album
            ]);
        }
    }

    public function sitemap()
    {
        $sitemap = app()->make('sitemap');


        $sitemap = \App::make('sitemap');
        // add home pages mặc định
        $sitemap->add(\URL::to('/'), Carbon::now(), 1, 'daily');

        // add bài viết
        $posts = \DB::table('posts')->orderBy('created_at', 'desc')->get();
        foreach ($posts as $post) {
            //$sitemap->add(url, thời gian, độ ưu tiên, thời gian quay lại)
            $sitemap->add(route('view_postDetail', [$post->slug, $post->id]), $post->updated_at, 1, 'daily');
        }



        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        $sitemap->store('xml', 'sitemap');
        if (\File::exists(public_path('sitemap.xml'))) {
            chmod(public_path('sitemap.xml'), 0777);
        }
    }
}