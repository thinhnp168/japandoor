<?php

namespace App\Http\Controllers;

use App\Agency;
use App\Province;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    public function listAgency()
    {
        $listProvince = Province::all();
        $listAgency = Agency::where('status', 1)->paginate(10);
        return view('client.agency.list', compact('listAgency', 'listProvince'));
    }
    public function listAgencyInProvince($slug, $id)
    {
        $listProvince = Province::all();
        $listAgency = Agency::where('status', 1)->where('province_id', $id)->paginate(10);
        return view('client.agency.list-in-province', compact('listAgency', 'listProvince', 'id'));
    }
    public function detailAgency($slug, $id)
    {
        $agency = Agency::find($id);
        return view('client.agency.detail', compact('agency'));
    }
}
