<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddSliderRequest;
use App\Http\Requests\EditSliderRequest;
use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function listSlider()
    {
        $listSlider = Slider::all();
        return view('admin.slider.list', compact('listSlider'));
    }
    public function formAddSlider()
    {
        return view('admin.slider.add');
    }
    public function addSlider(AddSliderRequest $request)
    {
        $addSlider = new Slider;
        $addSlider->name = $request->name;
        $addSlider->link = $request->link;
        $addSlider->status = $request->status;
        $addSlider->image = $request->image;
        $addSlider->save();
        return redirect()->route('listSlider')->with('success', 'Thêm mới thành công');
    }
    public function changeInfoSlider(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $slider = Slider::find($id);
        if ($slider->$type == 1) {
            $slider->$type = 0;
        } else {
            $slider->$type = 1;
        }
        $slider->save();
        if ($slider->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeValueSlider(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $type = $request->type;
        $slider = Slider::find($id);
        $slider->$type = $value;
        $slider->save();
        if ($slider->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditSlider($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit', compact('slider'));
    }
    public function editSlider($id, EditSliderRequest $request)
    {
        $editSlider = Slider::find($id);
        $editSlider->name = $request->name;
        $editSlider->status = $request->status;
        $editSlider->link = $request->link;
        $editSlider->image = $request->image;
        $editSlider->save();
        return redirect()->route('listSlider')->with('success', 'Chỉnh sửa thành công');
    }
    public function deleteSlider($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        return redirect()->route('listSlider')->with('success', 'Xoá thành công');
    }
}