<?php

namespace App\Http\Controllers\admin;

use App\Email;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function listEmail()
    {
        $listEmail = Email::all();
        return view('admin.email.list', compact('listEmail'));
    }
}