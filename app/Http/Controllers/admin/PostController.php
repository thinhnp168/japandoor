<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddPostRequest;
use App\Http\Requests\EditPostRequest;
use App\CategoryPost;
use App\Post;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{
    public function listPost()
    {
        $listPost = Post::all();
        return view('admin.post.list', compact('listPost'));
    }
    public function formAddPost()
    {
        $category = CategoryPost::where('status', 1)->where('type', 1)->get();
        return view('admin.post.add', compact('category'));
    }
    public function addPost(AddPostRequest $request)
    {
        $addPost = new Post;
        $addPost->name = $request->name;
        $addPost->summary = $request->summary;
        $addPost->slug = Str::slug($request->name);
        $addPost->image = $request->image;
        $addPost->status = $request->status;
        $addPost->highlight = $request->highlight;
        $addPost->user_id = Auth::user()->id;
        $addPost->category_id = $request->category_id;
        $addPost->title = $request->title;
        $addPost->description = $request->description;
        $addPost->keyword = $request->keyword;
        $addPost->content = $request->content;
        $addPost->save();
        return redirect()->route('listPost')->with('success', 'Thêm mới thành công');
    }
    public function changeInfoPost(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $post = Post::find($id);
        if ($post->$type == 1) {
            $post->$type = 0;
        } else {
            $post->$type = 1;
        }
        $post->save();
        if ($post->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeArrangePost(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $post = Post::find($id);
        $post->arrange = $value;
        $post->save();
        if ($post->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditPost($id)
    {
        $post = Post::find($id);
        $category = CategoryPost::where('status', 1)->where('type', 1)->get();
        return view('admin.post.edit', compact('post', 'category'));
    }
    public function EditPost(EditPostRequest $request, $id)
    {
        $editPost = Post::find($id);
        $editPost->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $editPost->slug = Str::slug($request->name);
        } else {
            $editPost->slug = Str::slug($request->slug);
        }

        $editPost->summary = $request->summary;
        $editPost->image = $request->image;
        $editPost->status = $request->status;
        $editPost->highlight = $request->highlight;
        $editPost->user_id = Auth::user()->id;
        $editPost->category_id = $request->category_id;
        $editPost->title = $request->title;
        $editPost->description = $request->description;
        $editPost->keyword = $request->keyword;
        $editPost->content = $request->content;
        $editPost->save();
        return redirect()->route('listPost')->with('success', 'Chỉnh sửa thành công');
    }
    public function deletePost($id)
    {
        $deletePost = Post::find($id);
        $deletePost->delete();
        return redirect()->route('listPost')->with('success', 'Xoá thành công');
    }
}