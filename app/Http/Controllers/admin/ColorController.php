<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Color;
use App\Http\Requests\AddColorRequest;

class ColorController extends Controller
{
    public function listColor()
    {
        $listColor = Color::all();
        return view('admin.color.list', compact('listColor'));
    }
    public function formAddColor()
    {
        return view('admin.color.add');
    }
    public function addColor(AddColorRequest $request)
    {
        $addColor = new Color;
        $addColor->name = $request->name;
        $addColor->status = $request->status;
        $addColor->save();
        return redirect()->route('listColor')->with('success', 'Thêm mới thành công');
    }
    public function deleteColor($id)
    {
        $deleteColor = Color::find($id);
        $deleteColor->delete();
        return redirect()->route('listColor')->with('success', 'Xoá thành công');
    }
    public function changeInfoColor(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $color = Color::find($id);
        if ($color->$type == 1) {
            $color->$type = 0;
        } else {
            $color->$type = 1;
        }
        $color->save();
        if ($color->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeNameColor(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $color = Color::find($id);
        $color->name = $value;
        $color->save();
        if ($color->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
}