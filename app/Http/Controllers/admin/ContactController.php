<?php

namespace App\Http\Controllers\admin;

use App\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function listContact()
    {
        $listContact = Contact::all();
        return view('admin.contact.list', compact('listContact'));
    }
}