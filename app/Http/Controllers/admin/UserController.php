<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;

class UserController extends Controller
{
    public function listUser()
    {
        $listUser = User::all();

        return view('admin.user.list', compact('listUser'));
    }
    public function formAddUser()
    {
        return view('admin.user.add');
    }
    public function addUser(AddUserRequest $request)
    {
        $newUser = new User;

        $newUser->ten = $request->ten;
        $newUser->email = $request->email;
        $newUser->dienthoai = $request->dienthoai;
        $newUser->xuatban = $request->xuatban;
        $newUser->password = bcrypt($request->password);
        $newUser->save();
        return redirect()->route('listUsers')->with('success', 'Thêm mới thành công');
    }
    public function changeInfoUser(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);
        if ($user->xuatban == 1) {
            $user->xuatban = 0;
            $user->save();
        } else {
            $user->xuatban = 1;
            $user->save();
        }
        if ($user->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditUser($id)
    {
        $editUser = User::find($id);
        return view('admin.user.edit', compact('editUser'));
    }
    public function editUser($id, EditUserRequest $request)
    {
        $editUser = User::find($id);
        $editUser->ten = $request->ten;
        $editUser->email = $request->email;
        $editUser->dienthoai = $request->dienthoai;
        $editUser->xuatban = $request->xuatban;
        $editUser->save();
        return redirect()->route('listUsers')->with('success', 'Chỉnh sửa thành công');
    }
    public function changePassUser(Request $request)
    {

        $id = $request->id;
        $password = $request->password;
        $len = strlen($password);
        if ($len < 4) {
            return response()->json([
                'type' => 2,
                'mess' => 'Mật khẩu không được ít hơn 4 ký tự!'
            ]);
        }
        if ($len > 31) {
            return response()->json([
                'type' => 2,
                'mess' => 'Mật khẩu không được nhiều hơn 31 ký tự!'
            ]);
        }
        $user = User::find($id);
        $user->password = bcrypt($password);
        $user->save();
        if ($user->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Đổi mật khẩu thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function deleteUser($id)
    {
        $deleteUser = User::find($id);
        $deleteUser->delete();
        return redirect()->route('listUsers')->with('success', 'Xoá thành công');
    }
}