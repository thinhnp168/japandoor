<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Accessory;
use App\Http\Requests\AddAccessoryRequest;
use App\Http\Requests\EditAccessoryRequest;
use Illuminate\Support\Str;

class AccessoryController extends Controller
{
    public function listAccessory()
    {
        $listAccessory = Accessory::all();
        return view('admin.accessory.list', compact('listAccessory'));
    }
    public function formAddAccessory()
    {
        return view('admin.accessory.add');
    }
    public function addAccessory(AddAccessoryRequest $request)
    {
        $addAccessory = new Accessory;
        $addAccessory->name = $request->name;
        $addAccessory->slug = Str::slug($request->name);
        $addAccessory->code = $request->code;
        $addAccessory->price = $request->price;
        $addAccessory->summary = $request->summary;
        $addAccessory->status = $request->status;
        $addAccessory->highlight = $request->highlight;
        $addAccessory->image = $request->image;
        $addAccessory->title = $request->title;
        $addAccessory->description = $request->description;
        $addAccessory->keyword = $request->keyword;
        $addAccessory->save();
        return redirect()->route('listAccessory')->with('success', 'Thêm mới thành công');
    }
    public function changeInfoAccessory(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $accessory = Accessory::find($id);
        if ($accessory->$type == 1) {
            $accessory->$type = 0;
        } else {
            $accessory->$type = 1;
        }
        $accessory->save();
        if ($accessory->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditAccessory($id)
    {
        $accessory = Accessory::find($id);
        return view('admin.accessory.edit', compact('accessory'));
    }
    public function editAccessory($id, EditAccessoryRequest $request)
    {
        $editAccessory = Accessory::find($id);
        $editAccessory->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $editAccessory->slug = Str::slug($request->name, "-");
        } else {
            $editAccessory->slug = Str::slug($request->slug, "-");
        }
        $editAccessory->code = $request->code;
        $editAccessory->price = $request->price;
        $editAccessory->summary = $request->summary;
        $editAccessory->status = $request->status;
        $editAccessory->highlight = $request->highlight;
        $editAccessory->image = $request->image;
        $editAccessory->title = $request->title;
        $editAccessory->description = $request->description;
        $editAccessory->keyword = $request->keyword;
        $editAccessory->save();
        return redirect()->route('listAccessory')->with('success', 'Chỉnh sửa thành công');
    }
    public function deleteAccessory($id)
    {
        $deleteAccessory = Accessory::find($id);
        $deleteAccessory->delete();
        return redirect()->route('listAccessory')->with('success', 'Xoá thành công');
    }
}