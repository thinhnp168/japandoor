<?php

namespace App\Http\Controllers\admin;

use App\Agency;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddAgencyRequest;
use App\Http\Requests\AddProvinceRequest;
use App\Http\Requests\EditAgencyRequest;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AgencyController extends Controller
{
    public function listProvince()
    {
        $listProvince = Province::all();
        return view('admin.agency.list-province', compact('listProvince'));
    }
    public function formAddProvince()
    {
        return view('admin.agency.add-province');
    }
    public function addProvince(AddProvinceRequest $request)
    {
        $addProvince = new Province;
        $addProvince->name = $request->name;
        $addProvince->slug = Str::slug($request->name);
        $addProvince->region = $request->region;
        $addProvince->save();
        return redirect()->route('listProvince')->with('success', 'Thêm mới thành công!');
    }
    public function changeValueProvince(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $type = $request->type;
        $province = Province::find($id);
        if ($type = 'slug') {
            if ($value == '' || $value == null) {
                $province->$type = Str::slug($province->name);
                $province->save();
                if ($province->save()) {
                    return response()->json([
                        'type' => 3,
                        'value' => Str::slug($province->name),
                        'mess' => 'Thành công!'
                    ]);
                } else {
                    return response()->json([
                        'type' => 2,
                        'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
                    ]);
                }
            } else {
                $province->$type = Str::slug($value);
                $province->save();
                if ($province->save()) {
                    return response()->json([
                        'type' => 3,
                        'value' => Str::slug($value),
                        'mess' => 'Thành công!'
                    ]);
                } else {
                    return response()->json([
                        'type' => 2,
                        'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
                    ]);
                }
            }
        } else {
            $province->$type = $value;
            $province->save();
            if ($province->save()) {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            } else {
                return response()->json([
                    'type' => 2,
                    'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
                ]);
            }
        }
    }
    public function deleteProvince($id)
    {
        $deleteProvince = Province::find($id);
        $deleteProvince->delete();
        return redirect()->route('listProvince')->with('success', 'Xoá thành công!');
    }
    public function listAgency()
    {
        $listAgency = Agency::all();
        return view('admin.agency.list-agency', compact('listAgency'));
    }
    public function formAddAgency()
    {
        $province = Province::all();
        return view('admin.agency.add-agency', compact('province'));
    }
    public function addAgency(AddAgencyRequest $request)
    {
        $addAgency = new Agency;
        $addAgency->name = $request->name;
        $addAgency->slug = Str::slug($request->name);
        $addAgency->address = $request->address;
        $addAgency->phone = $request->phone;
        $addAgency->zalo = $request->zalo;
        $addAgency->facebook = $request->facebook;
        $addAgency->map = $request->map;
        $addAgency->status = $request->status;
        $addAgency->province_id = $request->province_id;
        $addAgency->title = $request->title;
        $addAgency->description = $request->description;
        $addAgency->keyword = $request->keyword;
        $addAgency->image = $request->image;
        $addAgency->album = $request->album;
        $addAgency->content = $request->content;
        $addAgency->save();
        return redirect()->route('listAgency')->with('success', 'Thêm mới thành công!');
    }
    public function formEditAgency($id)
    {
        $province = Province::all();
        $agency = Agency::find($id);
        return view('admin.agency.edit-agency', compact('province', 'agency'));
    }
    public function editAgency($id, EditAgencyRequest $request)
    {
        $editAgency = Agency::find($id);
        $editAgency->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $editAgency->slug = Str::slug($request->name);
        } else {
            $editAgency->slug = Str::slug($request->slug);
        }
        $editAgency->address = $request->address;
        $editAgency->phone = $request->phone;
        $editAgency->zalo = $request->zalo;
        $editAgency->facebook = $request->facebook;
        $editAgency->map = $request->map;
        $editAgency->status = $request->status;
        $editAgency->province_id = $request->province_id;
        $editAgency->title = $request->title;
        $editAgency->description = $request->description;
        $editAgency->keyword = $request->keyword;
        $editAgency->image = $request->image;
        $editAgency->album = $request->album;
        $editAgency->content = $request->content;
        $editAgency->save();
        return redirect()->route('listAgency')->with('success', 'Chỉnh sửa thành công!');
    }
    public function formDeleteAgency($id)
    {
        $agency = Agency::find($id);
        $agency->delete();
        return redirect()->route('listAgency')->with('success', 'Xoá thành công!');
    }
}