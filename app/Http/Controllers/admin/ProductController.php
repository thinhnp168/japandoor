<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Color;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\EditProductRequest;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function listProduct()
    {
        $listProduct = Product::all();
        return view('admin.product.list', compact('listProduct'));
    }
    public function changeInfoProduct(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $product = Product::find($id);
        if ($product->$type == 1) {
            $product->$type = 0;
        } else {
            $product->$type = 1;
        }
        $product->save();
        if ($product->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeValueProduct(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $type = $request->type;
        $product = Product::find($id);
        $product->$type = $value;
        $product->save();
        if ($product->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formAddProduct()
    {
        $color = Color::where('status', 1)->get();
        $category = Category::where('status', 1)->get();
        return view('admin.product.add', compact('color', 'category'));
    }
    public function addProduct(AddProductRequest $request)
    {
        $addProduct = new Product;
        $addProduct->name = $request->name;
        $addProduct->slug = Str::slug($request->name);
        $addProduct->code = $request->code;
        $addProduct->summary = $request->summary;
        $addProduct->price = $request->price;
        $addProduct->status = $request->status;
        $addProduct->highlight = $request->highlight;
        $addProduct->color_id = $request->color_id;
        $addProduct->category_id = $request->category_id;
        $addProduct->image = $request->image;
        $addProduct->title = $request->title;
        $addProduct->description = $request->description;
        $addProduct->keyword = $request->keyword;
        $addProduct->album = $request->album;
        $addProduct->content = $request->content;
        $addProduct->warranty = $request->warranty;
        $addProduct->save();
        return redirect()->route('listProduct')->with('success', 'Thêm mới thành công!');
    }
    public function formEditProduct($id)
    {
        $color = Color::where('status', 1)->get();
        $category = Category::where('status', 1)->get();
        $product = Product::find($id);
        return view('admin.product.edit', compact('color', 'category', 'product'));
    }
    public function editProduct($id, EditProductRequest $request)
    {
        $editProduct = Product::find($id);
        $editProduct->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $editProduct->slug = Str::slug($request->name);
        } else {
            $editProduct->slug = Str::slug($request->slug);
        }
        $editProduct->code = $request->code;
        $editProduct->summary = $request->summary;
        $editProduct->price = $request->price;
        $editProduct->status = $request->status;
        $editProduct->highlight = $request->highlight;
        $editProduct->color_id = $request->color_id;
        $editProduct->category_id = $request->category_id;
        $editProduct->image = $request->image;
        $editProduct->title = $request->title;
        $editProduct->description = $request->description;
        $editProduct->keyword = $request->keyword;
        $editProduct->album = $request->album;
        $editProduct->content = $request->content;
        $editProduct->warranty = $request->warranty;
        $editProduct->save();
        return redirect()->route('listProduct')->with('success', 'Chỉnh sửa thành công!');
    }
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('listProduct')->with('success', 'Xoá thành công!');
    }
}