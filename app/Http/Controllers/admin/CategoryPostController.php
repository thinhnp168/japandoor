<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CategoryPost;
use App\Http\Requests\AddCategoryPostRequest;
use App\Http\Requests\EditCategoryPostRequest;
use Illuminate\Support\Str;

class CategoryPostController extends Controller
{
    public function listCategoryPost()
    {
        $listCategoryPost = CategoryPost::all();
        return view('admin.categorypost.list', compact('listCategoryPost'));
    }
    public function formAddCategoryPost()
    {
        return view('admin.categorypost.add');
    }
    public function AddCategoryPost(AddCategoryPostRequest $request)
    {
        $addCategoryPost = new CategoryPost;
        $addCategoryPost->name = $request->name;
        $addCategoryPost->slug = Str::slug($request->name, "-");
        $addCategoryPost->type = $request->type;
        $addCategoryPost->status = $request->status;
        $addCategoryPost->image = $request->image;
        $addCategoryPost->title = $request->title;
        $addCategoryPost->description = $request->description;
        $addCategoryPost->keyword = $request->keyword;
        $addCategoryPost->content = $request->content;
        $addCategoryPost->save();
        return redirect()->route('listCategoryPost')->with('success', 'Thêm mới thành công');
    }
    public function changeInfoCategoryPost(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $categoryPost = CategoryPost::find($id);
        if ($categoryPost->$type == 1) {
            $categoryPost->$type = 0;
        } else {
            $categoryPost->$type = 1;
        }
        $categoryPost->save();
        if ($categoryPost->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditCategoryPost($id)
    {
        $categoryPost = CategoryPost::find($id);
        return view('admin.categorypost.edit', compact('categoryPost'));
    }
    public function EditCategoryPost(EditCategoryPostRequest $request, $id)
    {
        $EditCategoryPost = CategoryPost::Find($id);
        $EditCategoryPost->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $EditCategoryPost->slug = Str::slug($request->name, "-");
        } else {
            $EditCategoryPost->slug = Str::slug($request->slug, "-");
        }
        $EditCategoryPost->type = $request->type;
        $EditCategoryPost->status = $request->status;
        $EditCategoryPost->image = $request->image;
        $EditCategoryPost->title = $request->title;
        $EditCategoryPost->description = $request->description;
        $EditCategoryPost->keyword = $request->keyword;
        $EditCategoryPost->content = $request->content;
        $EditCategoryPost->save();
        return redirect()->route('listCategoryPost')->with('success', 'Chỉnh sửa thành công');
    }
    public function deleteCategoryPost($id)
    {
        $deleteCategoryPost = CategoryPost::find($id);
        $deleteCategoryPost->delete();
        return redirect()->route('listCategoryPost')->with('success', 'Xoá thành công');
    }
}