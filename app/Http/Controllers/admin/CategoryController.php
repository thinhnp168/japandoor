<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function listCategory()
    {
        $listCategory = Category::all();
        return view('admin.category.list', compact('listCategory'));
    }
    public function formAddCategory()
    {
        return view('admin.category.add');
    }
    public function addCategory(AddCategoryRequest $request)
    {
        $addCategory = new Category;
        $addCategory->name = $request->name;
        $addCategory->slug = Str::slug($request->name);
        $addCategory->price = $request->price;
        $addCategory->status = $request->status;
        $addCategory->image = $request->image;
        $addCategory->title = $request->title;
        $addCategory->description = $request->description;
        $addCategory->keyword = $request->keyword;
        $addCategory->content = $request->content;
        $addCategory->save();
        return redirect()->route('listCategory')->with('success', "Thêm mới thành công!");
    }
    public function changeInfoCategory(Request $request)
    {
        $id = $request->id;
        $type = $request->type;
        $category = Category::find($id);
        if ($category->$type == 1) {
            $category->$type = 0;
        } else {
            $category->$type = 1;
        }
        $category->save();
        if ($category->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeValueCategory(Request $request)
    {
        $id = $request->id;
        $value = $request->value;
        $type = $request->type;
        $category = Category::find($id);
        $category->$type = $value;
        $category->save();
        if ($category->save()) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditCategory($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit', compact('category'));
    }
    public function editCategory($id, EditCategoryRequest $request)
    {
        $editCategory = Category::find($id);
        $editCategory->name = $request->name;
        if ($request->slug == '' || $request->slug == null) {
            $editCategory->slug = Str::slug($request->name);
        } else {
            $editCategory->slug = Str::slug($request->slug);
        }
        $editCategory->price = $request->price;
        $editCategory->status = $request->status;
        $editCategory->image = $request->image;
        $editCategory->title = $request->title;
        $editCategory->description = $request->description;
        $editCategory->keyword = $request->keyword;
        $editCategory->content = $request->content;
        $editCategory->save();
        return redirect()->route('listCategory')->with('success', "Chỉnh sửa thành công!");
    }
    public function deleteCategory($id)
    {
        $deleteCategory = Category::find($id);
        $deleteCategory->delete();
        return redirect()->route('listCategory')->with('success', "Xoá thành công!");
    }
}