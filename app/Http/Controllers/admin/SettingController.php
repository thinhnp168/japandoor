<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditSettingRequest;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function formEditSetting()
    {
        $editSetting = Setting::find(1);
        return view('admin.setting.edit', compact('editSetting'));
    }
    public function editSetting(EditSettingRequest $request)
    {
        $editSetting = Setting::find(1);
        $editSetting->name = $request->name;
        $editSetting->email = $request->email;
        $editSetting->phone = $request->phone;
        $editSetting->address = $request->address;
        $editSetting->image = $request->image;
        $editSetting->title = $request->title;
        $editSetting->description = $request->description;
        $editSetting->keyword = $request->keyword;
        $editSetting->save();
        return redirect()->back()->with('success', "Lưu lại thành công!");
    }
}