<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function listOrder()
    {
        $listOrder = Order::all();
        return view('admin.order.list', compact('listOrder'));
    }
}