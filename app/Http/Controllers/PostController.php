<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryPost;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function postCategoryPost($slug, $id)
    {
        $listCategory = Category::where('status', 1)->get();
        $listCategoryPost = CategoryPost::where('status', 1)->where('type', 1)->get();
        $category = CategoryPost::with('post')->find($id);
        $listPost = $category->post()->paginate(15);
        $list_most_view = Post::getPostMostView(5);
        if ($category->type == 0) {
            return view('client.post.category-singlepage', compact('category', 'listCategoryPost'));
        } else {
            return view('client.post.category', compact('category', 'listCategory', 'listPost', 'list_most_view'));
        }
    }
    public function postDetail($slug, $id)
    {
        $listCategoryPost = CategoryPost::where('status', 1)->where('type', 1)->get();
        $post = Post::with('category')->find($id);
        $post->view = $post->view + 1;
        $post->save();
        return view('client.post.detail', compact('post', 'listCategoryPost'));
    }
}