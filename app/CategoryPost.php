<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class CategoryPost extends Model
{
    public function post()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
}
