<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\ServiceProvider;
use App\Setting;
use App\Post;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $category_share = Category::where('status', 1)->get();
        $setting_share = Setting::find(1);
        $postMostView_share = Post::getPostMostView(5);
        View::share('setting_share', $setting_share);
        View::share('postMostView_share', $postMostView_share);
        View::share('category_share', $category_share);
    }
}
