// $(document).on("click", "#pills-profile-tab", function() {
//     $('#slide-cuaso').lightSlider({
//         gallery: true,
//         item: 1,
//         thumbItem: 4,
//         slideMargin: 0,
//         speed: 500,
//         auto: true,
//         loop: true,
//         onSliderLoad: function() {
//             $('#slide-cuaso').removeClass('cS-hidden');
//         }
//     });
// });
function formatCurrency(number) {
    var n = number.toString().split('').reverse().join("");
    var n2 = n.replace(/\d\d\d(?!$)/g, "$&.");
    return n2.split('').reverse().join('') + ' VNĐ';
}
// $(document).on("change", "#loaicua", function () {
//     let id = $(this.options[this.selectedIndex]).attr('data-id');
//     $('#loaicua').attr('data-idloaicuachon', id);
//     console.log(this);

// });
$(document).on("click", "#t-btn-tinhtien", function() {
    let loaicua = parseFloat($("#loaicua").val());
    let maucua = parseFloat($("#maucua").val());
    let chieurong = parseFloat($("#chieurong").val());
    let chieucao = parseFloat($("#chieucao").val());
    let phukien = parseFloat($("#phukien").val());
    let dodaytuong = parseFloat($("#dodaytuong").val());
    let giathanh = loaicua * chieurong * chieucao + phukien + (500000 * dodaytuong);
    let giathanhtext = formatCurrency(giathanh);
    let id_loaicuachon = $('#loaicua').find(':selected').data('id');
    $('#ketqua').val(giathanhtext);
    if (id_loaicuachon == 0) {
        $('#t-bao-slide-locsp').html('<h5>Bạn chưa chọn sản phẩm hoặc sản phẩm chưa có ảnh phù hợp</h5>')
        return false;
    }
    data_ = {
        'id_loaicuachon': id_loaicuachon,
        'id_maucua': maucua,
        'type': 'tim-cua'
    }
    var request = $.ajax({
        url: "/ajax/cart.php",
        type: "POST",
        data: data_,
        dataType: "json"
    });
    request.done(function(msg) {
        if (msg != null) {
            let imageGallerysanpham = '<ul class="imageGallerysanpham">';
            $.each(msg, function(k, v) {
                imageGallerysanpham += '<li data-thumb="' + v.file + '"><img width="100%" src="' + v.file + '"/></li>';
            });
            imageGallerysanpham += "</ul>";
            $('#t-bao-slide-locsp').html(imageGallerysanpham);
            $('.imageGallerysanpham').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('.imageGallerysanpham').removeClass('cS-hidden');
                }
            });
        } else {
            let tbao = "<p>Sản phẩm chưa có thông tin </p>"
            $('#t-bao-slide-locsp').html(tbao);
        }

        return false;
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
    });
});

$(document).on('click', '#guithongtindathang', function(e) {
    e.preventDefault();
    let _this = $(this);
    // _this.prop('disabled', true);
    let hoten = $('#hovaten').val();
    let sodienthoai = $('#sodienthoai').val();
    let thoigianthuchien = $('#thoigianthuchien').val();
    let soluongcuaso = $('#soluongcuaso').val();
    let thanhpho = $('#thanhpho').val();
    let loaiduan = $('#loaiduan').val();
    let thongtinkhac = $('#thongtinkhac').val();
    toastr.clear();
    toastr.options = {
        "closeButton": true,
        "timeOut": "500000",
        "positionClass": "toast-top-right"
    }
    if (hoten == '') {
        toastr.warning("Vui lòng điền vào họ và tên");
        _this.prop('disabled', false);
        return false;
    }
    if (thanhpho == '') {
        toastr.warning("Vui lòng điền vào tỉnh/thành phố.");
        _this.prop('disabled', false);
        return false;
    }
    data_ = {
        'hoten': hoten,
        'sodienthoai': sodienthoai,
        'thoigianthuchien': thoigianthuchien,
        'soluongcuaso': soluongcuaso,
        'thanhpho': thanhpho,
        'loaiduan': loaiduan,
        'thongtinkhac': thongtinkhac,
        'type': 'dathangnhanh'
    }
    var request = $.ajax({
        url: "/desktop/ajax/cart.php",
        type: "POST",
        data: data_,
        dataType: "json",
        headers: {
            "cache-control": "no-cache"
        },
        cache: false
    });
})