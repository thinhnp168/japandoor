<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('summary')->nullable();
            $table->string('slug');
            $table->string('image');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('keyword')->nullable();
            $table->integer('arrange')->default(0);
            $table->longText('content')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('category_posts')->onDelete('set null');
            $table->bigInteger('view')->default(0);
            $table->tinyInteger('highlight')->default(0)->comment('0: not highlight ; 1: highlight');
            $table->tinyInteger('status')->default(1)->comment('0: disable; 1: enable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}