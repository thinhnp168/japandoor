<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('image');
            $table->text('summary');
            $table->string('code');
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->bigInteger('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('set null');
            $table->tinyInteger('highlight')->default(0)->comment('0: not highlight ; 1: highlight');
            $table->tinyInteger('status')->default(1)->comment('0: disable; 1: enable');
            $table->integer('arrange')->default(0);
            $table->integer('price')->nullable();
            $table->longText('content')->nullable();
            $table->longText('warranty')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('keyword')->nullable();
            $table->text('album')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}