<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('slug');
            $table->string('image')->nullable();
            $table->integer('price')->nullable();
            $table->text('summary')->nullable();
            $table->tinyInteger('highlight')->default(0)->comment('0: not highlight ; 1: highlight');
            $table->tinyInteger('status')->default(1)->comment('0: disable; 1: enable');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('keyword')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories');
    }
}