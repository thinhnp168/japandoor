@extends('client.layout.index')
@section('title', $product->name )
@section('description', $product->description )
@section('keywords', $product->keyword )
@section('content')
<div class="container-fluid">
    <div class="container">
        <nav aria-label="breadcrumb" class="mt-3">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="/$rewrite_danhmuc-cp$iddanhmuc.html">$tendanhmuc</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-12 col-md-4">
                <ul id="imageGallerysanpham" class="t-list-image-ctsp">
                    @foreach (json_decode($product->album) as $item)
                    <li data-thumb="{{ $item }}">
                        <img width="100%" src="{{ $item }}" />
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-md-8 t-chitietsanpham">
                <h1 class="title-detail-product">{{ $product->name }}</h1>
                <div class="motangan">
                    {!! $product->summary !!}
                </div>
                <div>Mã sản phẩm : <strong>{{ $product->code }}</strong></div>
                <div class="gia mt-2 mb-2">Giá : <strong> Liên hệ qua các kênh dưới đây</strong> </div>

                <div class="socials-share">
                    <a class="bg-facebook" href="https://www.facebook.com/C%E1%BB%ADa-th%C3%A9p-Japan-153864021331780/" target="_blank"><span class="fa fa-facebook"></span> Fanpage</a>
                    <a class="bg-google-plus" href="tel:{{ $setting_share->phone }}" target="_blank"><span class="fa fa-phone"></span> Hotline: {{ $setting_share->phone }}</a>
                    <a class="bg-twitter" href="https://zalo.me/{{ $setting_share->phone }}" target="_blank"> <span class="fa fa-mobile"></span> Zalo</a>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Thông tin sản phẩm</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Chính sách bảo hành</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="chitietsanpham">
                            {!! $product->content !!}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="chitietsanpham">
                            {!! $product->warranty !!}
                        </div>
                    </div>
                </div>
                <!-- <p class="title-info-product mt-3 mb-3">Thông tin sản phẩm</p>
                 -->

            </div>
        </div>
    </div>
</div>
@endsection
