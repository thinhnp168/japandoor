@extends('client.layout.index')
@section('tilte', 'Danh sách sản phẩm')
@section('content')
<div class="container">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sản phẩm</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-8">
            <h1 class="tieude-danhmuc-sanpham">Sản phẩm</h1>
            <div class="row">
                @foreach ($allProduct as $item)
                    <div class="col-12 col-md-4 t-bao-sanpham-danhmucsanpham">
                        <a href="{{ route('view_detailProduct', [$item->slug, $item->id]) }}" title="{{ $item->name }}">
                            <div class="card t-card-sanpham t-card-sanpham-home">
                                <img src="{{ $item->image }}" class="card-img-top" alt="{{ $item->name }}"> 
                                <div class="card-body border">
                                <div class="input-rating">
                                    <i class="fa fa-star" data-value="1" ></i>
                                    <i class="fa fa-star" data-value="2" ></i>
                                    <i class="fa fa-star" data-value="3" ></i>
                                    <i class="fa fa-star" data-value="4" ></i>
                                    <i class="fa fa-star" data-value="5" ></i>
                                </div>
                                <h5 class="card-title">{{ $item->name }}</h5>
                                <p class="t-masanpham">Mã sản phẩm: {{ $item->code }}</p>
                                <p class="t-masanpham">Giá bán lẻ: {{ number_format($item->price, 0, '', '.') }} VND</p>
                                <a href="{{ route('view_detailProduct', [$item->slug, $item->id]) }}" title="{{ $item->name }}" class="btn t-btn-chrimson t-btn-xemchitietsp d-block">Xem chi tiết</a>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                


                {!! $allProduct->render() !!}
            </div>
        </div>
        <div class="col-md-4 ">
            <div class="right-bar">
                <h2 class="tieude-danhmuc-sanpham">Danh mục sản phẩm</h2>
                <li class="list-group-item"><a href="/">Trang chủ</a></li>
                @foreach ($listCategory as $item)
                    <li class="list-group-item"><a href="{{ route('view_listProduct', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                @endforeach
            </div>
            
        </div>
    </div>
</div>
@endsection
