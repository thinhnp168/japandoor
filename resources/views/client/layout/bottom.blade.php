<div class="container-fluid">
    <div class="container">
        <form class="form-inline mt-2">
            <div class="form-group mb-2">
                <p class="m-0">Nhập email để nhận khuyến mại</p>
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <label for="t-email-theodoi" class="sr-only">Email</label>
                <input type="email" class="form-control" id="t-email-theodoi" placeholder="Email">
            </div>
            <button type="submit" class="btn btn-primary mb-2 btn-submit-dangky-email">Đăng ký</button>
        </form>
    </div>
</div>
<div class="container-fluid t-footer">
    <div class="container">
        <div class="col-12">
            <div class="row ">
                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <p class="title">Thông tin công ty</p>
                            <h6 class="font-weight-bold ">{{ $setting_share->name }}</h6>
                            <p>Địa chỉ: {{ $setting_share->address }}</p>
                            <p>Email : {{ $setting_share->email }} </p>
                            <p>Hotline : {{ $setting_share->phone }} </p>
                        </div>
                        <div class="col-12 col-md-3">
                            <p class="title">Tin tức nổi bật</p>
                            <ul>
                                @foreach ($postMostView_share as $item)
                                <li class="t-footer-list pt-1"><a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-12 col-md-3">
                            <p class="title">Sản phẩm, dịch vụ</p>
                            <ul>
                                @foreach ($category_share as $item)
                                <li class="pt-1"><a href="{{ route('view_listProduct', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-3 ">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FC%25E1%25BB%25ADa-th%25C3%25A9p-Japan-153864021331780%2F&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                </div>
            </div>
        </div>

    </div>
</div>