<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        @hasSection('title')
            @yield('title')
        @else
            {{ $setting_share->title }}
        @endif
    </title>
    <link rel="canonical" href="" />
    <link rel="alternate" href="" hreflang="vi-vn" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <meta name="description" content="
                                        @hasSection('description')
                                            @yield('description')
                                        @else
                                            {{ $setting_share->description }}
                                        @endif
                                    " />
    <meta name="keywords" content="
                                    @hasSection('keywords')
                                        @yield('keywords')
                                    @else
                                        {{ $setting_share->keyword }}
                                    @endif
                                    
                                " />
    <meta name="robots" content="index" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no">
    <meta property="og:image" content="
                                    @hasSection('image')
                                        @yield('image')
                                    @else
                                        {{ $setting_share->image }}
                                    @endif
    " />
    <meta property="og:image:width" content="480" />
    <meta property="og:image:height" content="270" />
    <meta property="og:title" content="@hasSection('title')
                                            @yield('title')
                                        @else
                                            {{ $setting_share->title }}
                                        @endif" />
    <meta property="og:description" content="
                                                @hasSection('description')
                                                    @yield('description')
                                                @else
                                                    {{ $setting_share->description }}
                                                @endif
                                            " />
    <meta property="og:url" content="
                                            @hasSection('url')
                                                @yield('url')
                                            @else
                                                {{ url()->current() }}
                                            @endif
    " />
    <meta property="og:type" content="website" />
    <meta name="__token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="/client/asset/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/client/asset/css/owl.theme.default.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Gelasio:ital,wght@1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300&display=swap" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/client/asset/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="/client/asset/css/style.css" />
    
    <link type="text/css" rel="stylesheet" href="/client/asset/slide/css/lightslider.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/client/asset/css/fontawesome-all.min.css" />
    <link type="text/css" rel="stylesheet" href="/client/asset/css/toastr.min.css" />
    <link rel="stylesheet" media="screen and (max-width: 768px)" href="/client/asset/css/responsive-mobile.css" />
    <link rel="icon" type="image/png" sizes="16x16"  href="/client/asset/images/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
   
</head>

<body> 
    @include('client.layout.top')
    @yield('content')
    @include('client.layout.bottom')
    

    {{-- <div class="modal fade" id="popup-config" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">$tieude_popup</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                $noidung_popup
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div> --}}
      

    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="/client/asset/jquery.cookie.js"></script>
    <script src="/client/asset/js/owl.carousel.min.js"></script>
    <script src="/client/asset/slide/js/lightslider.js"></script>
    <script src="/client/asset/js/toastr.min.js"></script>
    <script>
        $(document).ready(function() {

            
            $('.main-slide').owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                dots: false,
                lazyLoad: true,
            })
            $('#imageGallery').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('#imageGallery').removeClass('cS-hidden');
                }
            });
            $('.slide-sanpham').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('.slide-sanpham').removeClass('cS-hidden');
                }
            });
            $('#slide-cuaso').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('#slide-cuaso').removeClass('cS-hidden');
                }
            });
            $('.imageGallerysanpham').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('.imageGallerysanpham').removeClass('cS-hidden');
                }
            });
            $('#imageGallerysanpham').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('#imageGallerysanpham').removeClass('cS-hidden');
                }
            });
            $('.t-album-anh-daily').lightSlider({
                gallery: true,
                item: 1,
                thumbItem: 4,
                slideMargin: 0,
                speed: 500,
                auto: true,
                loop: true,
                onSliderLoad: function() {
                    $('.t-album-anh-daily').removeClass('cS-hidden');
                }
            });
                
        });
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 50) {
                $('.topnumberone').addClass('fixed');
            } else {
                $('.topnumberone').removeClass('fixed');
            }
        });
        $(document).on('click', '.btn-submit-dangky-email', function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "50000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let email = $('#t-email-theodoi').val();
            if (email == '' || email == null) {
                toastr.warning('Email không để trống!');
                $(__this).prop('disabled', false);
                return false;
            } else {
                const __token = $('meta[name="__token"]').attr('content');
                data_ = {
                    _token: __token,
                    email : email 
                }
                let url = @json(route('saveEmail'));
                let request = $.ajax({
                url: url,
                type: "POST",
                data: data_,
                dataType: "json"
                });
                request.done(function (msg) {
                    if (msg.type == 1) {
                        toastr.success(msg.mess);
                        $('#t-email-theodoi').val('');
                        $(__this).prop('disabled', false);
                    }else {
                        toastr.warning(msg.mess);
                        $(__this).prop('disabled', false);
                    }
                    return false;
                });

                request.fail(function (jqXHR, textStatus) {
                    alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                });
            }
        });
    </script>
    @stack('scripts')
</body>

</html>