<div class="container-fluid topnumberone">
    <div class="container">
        <div class="row ">
            <div class="col-12 t-head">
                <nav class="navbar navbar-expand-lg navbar-light t-bg-mainnav ">
                        <a class="navbar-brand" href="/"><img width="50" height="50" src="{{ $setting_share->image }}" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="/" title="">Trang chủ</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_postCategoryPost', ['gioi-thieu', 2]) }}" title="Giới thiệu">Giới thiệu</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_allProduct') }}" title="Sản phẩm">Sản phẩm</span></a>
                                    <ul class="sub-menu-top2">
                                        @foreach ($category_share as $item)
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('view_listProduct', [$item->slug, $item->id]) }}" title="{{ $item->name }}"></span>{{ $item->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_postCategoryPost', ['video', 3]) }}" title="">Video</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_listAgency') }}" title="Đại lý">Đại lý</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_postCategoryPost', ['tin-tuc', 1]) }}" title="Tin tức">Tin tức</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('view_postCategoryPost', ['cong-trinh-tieu-bieu', 4]) }}" title="Công trình tiêu biểu">Công trình tiêu biểu</span></a>
                                </li>
                                <li class="nav-item sub-menu-top1">
                                    <a class="nav-link" href="{{ route('contact') }}" title="Liên hệ - Bản đồ">Liên hệ - Bản đồ</span></a>
                                </li>
                            </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    
</div>
<div class="t-bao-social">
    <a target="_blank" href="https://zalo.me/{{ $setting_share->phone }}">
        <span class="icon icon-zalo"></span>
    </a>
    <a target="_blank" href="tel:{{ $setting_share->phone }}">
        <span class="icon icon-phone"></span>
    </a>
    <a target="_blank" href="https://www.facebook.com/C%E1%BB%ADa-C%C3%B4ng-Ngh%E1%BB%87-Nh%E1%BA%ADt-B%E1%BA%A3n-Japan-153864021331780">
        <span class="icon icon-facebook"></span>
    </a>
</div>