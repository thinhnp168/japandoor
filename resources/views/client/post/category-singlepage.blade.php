@extends('client.layout.index')
@section('title', $category->name )
@section('description', $category->description )
@section('keywords', $category->keyword )
@section('content')
<div class="container ">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $category->ten }}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-md-8 box-detail-news">
                <h1 class="tieude-danhmuc-sanpham">{{ $category->ten }}</h1>
                <div class="chitiet-tintuc">
                    {!! $category->content !!}
                </div>
        </div>
        <div class="col-md-4 ">
            <div class="right-bar">
                <h2 class="tieude-danhmuc-sanpham">Danh mục tin</h2>
                <li class="list-group-item"><a href="/">Trang chủ</a></li>
                @foreach ($listCategoryPost as $item)
                    <li class="list-group-item"><a href="{{ route('view_postCategoryPost', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection