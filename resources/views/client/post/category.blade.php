@extends('client.layout.index')
@section('title', $category->name )
@section('description', $category->description )
@section('keywords', $category->keyword )
@section('content')
@php
    use Illuminate\Support\Str;
@endphp
<div class="container ">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $category->ten }}</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-12 col-md-8">
            <h1 class="tieude-danhmuc-sanpham">{{ $category->ten }}</h1>
            <div class="row">
                <div class="col-12">
                    @foreach ($listPost as $item)
                        <div class="card mb-3 t-card-danhmuctin" >
                            <div class="row ">
                                <div class="col-12 col-md-4 t-bao-anhbia">
                                    <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}"><img src="{{ $item->image }}" class="card-img" alt="{{ $item->name }}"></a>
                                </div>
                                <div class="col-12 col-md-8 t-bao-noidung">
                                    <div class="card-body">
                                    <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}"><h5 class="card-title font-weight-bold">{{ $item->name }}</h5></a>
                                    <p class="card-text ">{!! Str::limit($item->summary, 60, '...')  !!}</p>
                                    <p class="card-text"><small class="text-muted">{{  date_format($item->created_at, "d-m-Y") }}</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                {!! $listPost->render() !!}
            </div>
        </div>
        <div class="col-12 col-md-4 ">
            <div class="right-bar">
                <h2 class="tieude-danhmuc-sanpham">Danh mục sản phẩm</h2>
                <li class="list-group-item"><a href="/">Trang chủ</a></li>
                @foreach ($listCategory as $item)
                    <li class="list-group-item"><a href="{{ route('view_listProduct', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                @endforeach
            </div>
            <div class="right-bar pt-4">
                <h2 class="tieude-danhmuc-sanpham">Tin xem nhiều</h2>
                <li class="list-group-item"><a href="/">Trang chủ</a></li>
                @foreach ($list_most_view as $item)
                    <li class="list-group-item"><a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
