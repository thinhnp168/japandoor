@extends('client.layout.index')
@section('title', $post->name )
@section('description', $post->description )
@section('keywords', $post->keyword )
@section('content')
<div class="container">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="{{ route('view_postCategoryPost', [$post->category->slug, $post->category->id]) }}">{{ $post->category->name }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $post->name }}</li>
        </ol>
    </nav>
    <div class="row box-detail-news">
        <div class="col-md-8 ">
            <h1>{{ $post->name }}</h1>
            <div class="chitiet-tintuc cover-content">
                {!!  $post->content !!}
            </div>
            {{-- <h4 class="title-baivietlienquan">Bài viết liên quan</h4>
            <ul class="post-relate ">
                $baivietlienquan
            </ul> --}}
        </div>
        <div class="col-md-4">

            <div class="right-bar">
                <h2 class="tieude-danhmuc-sanpham">Danh mục tin</h2>
                <li class="list-group-item"><a href="/">Trang chủ</a></li>
                @foreach ($listCategoryPost as $item)
                    <li class="list-group-item"><a href="{{ route('view_postCategoryPost', [$item->slug, $item->id]) }}" rel="category tag" title="{{ $item->name }}">{{ $item->name }}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
