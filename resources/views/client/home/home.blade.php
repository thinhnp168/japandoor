@extends('client.layout.index')
@section('tilte', 'Japan Door')
@section('content')
@php
    use Illuminate\Support\Str;
@endphp
<div class="container-fluid">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="width: 100%;">
                    <div class="carousel-inner">
                        @php
                            $i = 0;
                        @endphp
                        @foreach ($slider as $item)
                            @php
                                $i++;
                                if ($i == 1) {
                                    $active = "active";
                                } else {
                                    $active = '';
                                }
                            @endphp
                            <div class="carousel-item {{ $active }}">
                                <a href="{{ $item->link }}" title="{{ $item->name }}">
                                    <img class="d-block w-100" src="{{ $item->image }}" alt="{{ $item->name }}" />
                                    <a href="{{ route('view_formOrder') }}" title="đặt hàng" class="btn-dathang btn btn-success"> ĐẶT HÀNG </a>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                        data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                        data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-2 t-bao-content-home d-flex flex-column align-items-center ">
        <h1 class="text-uppercase text-center"><span>C</span>hào mừng quý khách hàng đến với Japandoor</h1>
        <p>Japandoor là đơn vị cung cấp giải pháp toàn diện về cửa và phụ kiện, sản phẩm với kết cấu ưu việt chịu nước, chịu lực tốt nhất Việt Nam hiện tại. Với hệ thống đa kênh tại thị trường Việt Nam, Japandoor hy vọng mang tới sản phẩm tốt nhất, nhanh
            nhất đến quý khách hàng. </p>
    </div>
    <div class="container pt-2">
        <div class="t-bao-title">
            <a href="" class="t-head-title">Sản phẩm nổi bật</a>
        </div>
        <div class="row">
            @foreach ($category as $item)
            <div class="col-12 col-md-3">
                <div class="card t-card-sanpham t-card-sanpham-home">
                    <a href="{{ route('view_listProduct', [$item->slug, $item->id]) }}"><img src="{{ $item->image }}" class="card-img-top" alt="{{ $item->name }}"></a>
                    <div class="card-body border">
                        <div class="input-rating">
                            <i class="fa fa-star" data-value="1" ></i>
                            <i class="fa fa-star" data-value="2" ></i>
                            <i class="fa fa-star" data-value="3" ></i>
                            <i class="fa fa-star" data-value="4" ></i>
                            <i class="fa fa-star" data-value="5" ></i>
                        </div>
                        <h5 class="card-title">{{ $item->name }}</h5>
                        <div class="t-masanpham ">{!! $item->content !!}</div>
                        <a href="{{ route('view_listProduct', [$item->slug, $item->id]) }}" class="btn t-btn-chrimson t-btn-xemchitietsp d-block">Xem chi tiết</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="container pt-2">
        <div class="t-bao-title">
            <a href="" class="t-head-title">Dự tính chi phí</a>
        </div>
        <div class="row t-bao-form-tinhtien">
            <div class="col-12 col-md-7">
                <form method="POST" class="">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="loaicua">Chọn loại cửa</label>
                            <select name="" id="loaicua" class="form-control" data-idloaicuachon="0">
                                <option data-id="0" value="0">Chọn loại cửa</option>
                                @foreach ($category_caculator as $item)
                                    <option data-id="{{ $item->id }}" value="{{ $item->price }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="maucua">Chọn màu</label>
                            <select name="" id="maucua" class="form-control">
                                @foreach ($color_caculator as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="chieurong">Chiều rộng (m)</label>
                            <input class="form-control" type="number" name="" id="chieurong" min="0" value="1" placeholder="Nhập chiều rộng">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="chieucao">Chiều cao (m)</label>
                            <input class="form-control" type="number" name="" id="chieucao" min="0" value="1" placeholder="Nhập chiều cao">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="phukien">Chọn phụ kiện</label>
                            <select name="" id="phukien" class="form-control">
                                @foreach ($accessory_caculator as $item)
                                    <option value="{{ $item->price }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="dodaytuong">Độ dày tường</label>
                            <select name="" id="dodaytuong" class="form-control">
                                <option value="0">Nhỏ hơn hoặc bằng 240mm</option>
                                <option value="1">Lớn hơn 240mm</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for="ketqua">Giá dự kiến</label>
                            <input class="form-control" type="text" name="" id="ketqua" min="0">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="mb-0" for=""></label>
                            <button type="button" id="t-btn-tinhtien" class="btn t-btn-chrimson btn-block">Xác nhận</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-5" id="t-bao-slide-locsp">

            </div>
        </div>
    </div>
    <div class="container">
        <div class="t-bao-title">
            <a href="" class="t-head-title">Công trình tiêu biểu</a>
        </div>
        <div class="row">
            @foreach ($post_typical as $item)
                <div class="col-12 col-md-3">
                    <div class="card mb-3 ">
                        <div class="row t-card-tintuc-home">
                            <div class="col-12 t-bao-anhbia">
                                <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}">
                                    <img src="{{ $item->image }}" class="card-img" alt="{{ $item->name }}">
                                </a>
                            </div>
                            <div class="col-12  t-bao-noidung">
                                <div class="card-body">
                                    <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}">
                                        <h5 class="card-title font-weight-bold">{{ $item->name }}</h5>
                                    </a>
                                    <p class="card-text t-mota-tintuc-home">{{ $item->summary }}</p>
                                    <p class="card-text "><small class="text-muted">{{  date_format($item->created_at, "d-m-Y") }}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        <div class="t-bao-title">
            <a href="" class="t-head-title">Tin tức</a>
        </div>
        <div class="row">
            @foreach ($post_new as $item)
                <div class="col-12 col-md-3">
                    <div class="card mb-3 ">
                        <div class="row t-card-tintuc-home">
                            <div class="col-12 t-bao-anhbia">
                                <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}">
                                    <img src="{{ $item->image }}" class="card-img" alt="{{ $item->name }}">
                                </a>
                            </div>
                            <div class="col-12  t-bao-noidung">
                                <div class="card-body">
                                    <a href="{{ route('view_postDetail', [$item->slug, $item->id]) }}">
                                        <h5 class="card-title font-weight-bold">{{ $item->name }}</h5>
                                    </a>
                                    <p class="card-text t-mota-tintuc-home">{{ Str::limit($item->summary, 60, '...')  }}</p>
                                    <p class="card-text "><small class="text-muted">{{  date_format($item->created_at, "d-m-Y") }}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        function formatCurrency(number) {
            var n = number.toString().split('').reverse().join("");
            var n2 = n.replace(/\d\d\d(?!$)/g, "$&.");
            return n2.split('').reverse().join('') + ' VNĐ';
        }
        $(document).on('click', '#t-btn-tinhtien', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "50000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            
            $(__this).prop('disabled', true);
            let loaicua = parseFloat($("#loaicua").val());
            let maucua = parseFloat($("#maucua").val());
            let chieurong = parseFloat($("#chieurong").val());
            let chieucao = parseFloat($("#chieucao").val());
            let phukien = parseFloat($("#phukien").val());
            let dodaytuong = parseFloat($("#dodaytuong").val());
            let giathanh = loaicua * chieurong * chieucao + phukien + (500000 * dodaytuong);
            let giathanhtext = formatCurrency(giathanh);
            let id_loaicuachon = $('#loaicua').find(':selected').data('id');
            $('#ketqua').val(giathanhtext);
            if(id_loaicuachon == 0) {
                $('#t-bao-slide-locsp').html('<h5>Bạn chưa chọn sản phẩm hoặc sản phẩm chưa có ảnh phù hợp</h5>')
                $(__this).prop('disabled', false);
                return false;
            }
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id_loaicuachon : id_loaicuachon,
                id_maucua : maucua
            }
            let url = @json(route('searchProduct'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    let imageGallerysanpham = '<ul class="imageGallerysanpham">';
                    $.each(msg.album, function (k, v) {
                        imageGallerysanpham += '<li data-thumb="'+ v +'"><img width="100%" src="'+ v +'"/></li>';
                    });
                    imageGallerysanpham += "</ul>";
                    $('#t-bao-slide-locsp').html(imageGallerysanpham);
                    $('.imageGallerysanpham').lightSlider({
                        gallery: true,
                        item: 1,
                        thumbItem: 4,
                        slideMargin: 0,
                        speed: 500,
                        auto: true,
                        loop: true,
                        onSliderLoad: function() {
                            $('.imageGallerysanpham').removeClass('cS-hidden');
                        }
                    });
                    $(__this).prop('disabled', false);
                }else {
                    let tbao = "<p>Chưa có hình ảnh sản phẩm phù hợp!</p>"    
                    $('#t-bao-slide-locsp').html(tbao);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
    </script>
@endpush