@extends('client.layout.index')
@section('tilte', 'Đặt hàng')
@section('content')
<div class="container ">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">Đặt hàng</a></li>
        </ol>
    </nav>
    <h2 class="title mb-5">Thông tin đặt hàng</h2>
    <div class="row t-thongtin-dathang">

        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="hovaten">Họ và tên</label>
                <input type="text" class="form-control form-chrison ep-height" id="hovaten" placeholder="Nhập họ và tên">
            </div>
            <div class="form-group">
                <label for="sodienthoai">Số điện thoại</label>
                <input type="text" class="form-control form-chrison ep-height" id="sodienthoai" placeholder="Nhập số điện thoại">
            </div>
            <div class="form-group">
                <label for="thoigianthuchien">Thời gian thực hiện dự án (tháng)</label>
                <input type="number" min="1"  class="form-control form-chrison ep-height" id="thoigianthuchien" placeholder="1, 2, 3, ... 12">
            </div>
            <div class="form-group">
                <label for="soluongcuaso">Số lượng cửa sổ ước tính</label>
                <input type="number" min="0" class="form-control form-chrison ep-height" id="soluongcuaso">
            </div>
            <div class="form-group">
                <label for="soluongcua">Số lượng cửa ước tính</label>
                <input type="number" min="0" class="form-control form-chrison ep-height" id="soluongcua">
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="thanhpho">Thành phố</label>
                <input type="text" class="form-control form-chrison ep-height" id="thanhpho" placeholder="Nhập tỉnh/thành phố">
            </div>
            <div class="form-group">
                <label for="loaiduan">Loại dự án</label>
                <select name="" id="loaiduan" class="form-control form-chrison ep-height">
                    <option value="1">Nhà Dân</option>
                    <option value="2">Dự án</option>
                    <option value="3">Đại lý</option>
                </select>
            </div>
            <div class="form-group">
                <label for="thongtinkhac">Một vài thông tin khác</label>
                <textarea class="form-control form-chrison " name="" id="thongtinkhac" rows="12"></textarea>
            </div>
        </div>



    </div>
    <div class="row justify-content-center mb-5">
        <input class="btn btn-primary text-center" type="submit" id="guithongtindathang" value="Gửi thông tin đặt hàng">
    </div>
</div>
@endsection
@push('scripts')
    <script>
        function validate(phone) {
            const regex = /^(84|0[3|5|7|8|9])+([0-9]{8})\b$/;
            return regex.test(phone);
        }
        $(document).on('click', '#guithongtindathang', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "50000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let name = $('#hovaten').val();
            let phone = $('#sodienthoai').val();
            let month = $('#thoigianthuchien').val();
            let door = $('#soluongcua').val();
            let window = $('#soluongcuaso').val();
            let province = $('#thanhpho').val();
            let type = $('#loaiduan').val();
            let content = $('#thongtinkhac').val();

            if (name == '' || name == null) {
                toastr.warning('Tên không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (name.length > 150) {
                toastr.warning('Tên phải nhỏ hơn 150 ký tự!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (phone == '' || phone == null) {
                toastr.warning('Số điện thoại không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (!validate(phone)) {
                toastr.warning('Số điện thoại không đúng định dạng!');
                $(__this).prop('disabled', false);
                return false;
            } 
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                name : name,
                phone : phone,
                month : month,
                door : door,
                window : window,
                province : province,
                type : type,
                content : content
            }
            let url = @json(route('view_order'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    toastr.success(msg.mess);
                    $('#hovaten').val('');
                    $('#sodienthoai').val('');
                    $('#thoigianthuchien').val('');
                    $('#soluongcua').val('');
                    $('#soluongcuaso').val('');
                    $('#thanhpho').val('');
                    $('#loaiduan').val('');
                    $('#thongtinkhac').val('');
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
    </script>
@endpush