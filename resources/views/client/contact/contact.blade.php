@extends('client.layout.index')
@section('tilte', 'Thông tin liên hệ')
@section('content')
<div class="container ">
    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">Liên hệ</a></li>
        </ol>
    </nav>

    <div class="row t-thongtin-lienhe mb-4">
        <div class="col-12 col-md-6">
            <h1 class="title">Thông tin liên hệ</h1>
            <h5>Công ty TNHH SX & TM XNK Công nghệ Ánh Dương</h5>
            <p>Địa chỉ: Số 90 Tây Sơn, Thị trấn Phùng, Đan Phượng, Hà Nội</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7444.939212498865!2d105.657155!3d21.093836!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf74ee84284ca742e!2zS2h1IEPDtG5nIE5naGnhu4dwIEPhuqd1IEfDoW8!5e0!3m2!1svi!2sus!4v1633226123110!5m2!1svi!2sus"
                width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="tieude">Tiêu đề</label>
                <input type="text" class="form-control form-chrison ep-height" id="tieude" placeholder="Nhập tiêu đề">
            </div>
            <div class="form-group">
                <label for="hovaten">Họ tên</label>
                <input type="text" class="form-control form-chrison ep-height" id="hoten" placeholder="Nhập họ và tên">
            </div>
            <div class="form-group">
                <label for="sodienthoai">Số điện thoại</label>
                <input type="text" class="form-control form-chrison ep-height" id="sodienthoai" placeholder="Nhập số điện thoại">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control form-chrison ep-height" id="email" placeholder="Nhập email" required>
            </div>
            <div class="form-group">
                <label for="diachi">Địa chỉ</label>
                <input type="text" class="form-control form-chrison ep-height" id="diachi" placeholder="Nhập địa chỉ">
            </div>
            <div class="form-group">
                <label for="thongtinkhac">Ý kiến</label>
                <textarea class="form-control form-chrison " name="" id="thongtinkhac" rows="7"></textarea>
            </div>
            <input class="btn btn-primary text-center btn-guiykien" type="submit" id="btn-guiykien" value="Gửi ý kiến">
        </div>
    </div>

</div>
@endsection
@push('scripts')
    <script>
        function validate(phone) {
            const regex = /^(84|0[3|5|7|8|9])+([0-9]{8})\b$/;
            return regex.test(phone);
        }
        $(document).on('click','#btn-guiykien', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "50000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            
            $(__this).prop('disabled', true);

            let tieude = $('#tieude').val();
            let hoten = $('#hoten').val();
            let sodienthoai = $('#sodienthoai').val();
            let email = $('#email').val();
            let diachi = $('#diachi').val();
            let thongtinkhac = $('#thongtinkhac').val();
            if (tieude == '' || tieude == null) {
                toastr.warning('Tiêu đề không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (tieude.length > 100) {
                toastr.warning('Tiêu đề phải nhỏ hơn 100 ký tự!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (hoten == '' || hoten == null) {
                toastr.warning('Họ tên không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (hoten.length > 100) {
                toastr.warning('Họ tên phải nhỏ hơn 100 ký tự!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (!validate(sodienthoai)) {
                toastr.warning('Số điện thoại không đúng định dạng!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (email == '' || email == null) {
                toastr.warning('Email không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (email.length > 100) {
                toastr.warning('Chọn email khác!');
                $(__this).prop('disabled', false);
                return false;
            } 
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                tieude : tieude,
                hoten : hoten,
                sodienthoai : sodienthoai,
                email : email,
                diachi : diachi,
                thongtinkhac : thongtinkhac
            }
            let url = @json(route('saveContact'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    toastr.success(msg.mess);
                    $('#tieude').val('');
                    $('#hoten').val('');
                    $('#sodienthoai').val('');
                    $('#email').val('');
                    $('#diachi').val('');
                    $('#thongtinkhac').val('');
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
    </script>
@endpush
