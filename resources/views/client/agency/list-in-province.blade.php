@extends('client.layout.index')
@section('title', 'Hệ thống đại lý')
@section('content')
<div class="container-fluid">
    <div class="container">
        <nav aria-label="breadcrumb" class="mt-3">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hệ thống đại lý</li>
            </ol>
        </nav>
        <div class="row d-flex justify-content-center t-bao-title-danhsachdaily">
            <h1 class="d-block text-center font-weight-bold text-uppercase ">HỆ THỐNG ĐẠI LÝ PHỦ KÍN KHẮP CÁC TỈNH THÀNH TRÊN CẢ NƯỚC</h1>

        </div>

      <input type="hidden" value="{{ $id }}" id="activeLeft">
          

        <div class="row">
            <div class="col-12 col-md-4">
                <div class="list-group" id="myList" role="tablist">
                    <ul class="list-group mb-2">
                        <li class="list-group-item active sub1" >Miền Bắc
                            @foreach ($listProvince as $item)
                                @if ($item->region == 1)
                                <li class="list-group-item sub2-daily" data-id="{{ $item->id }}" ><a href="{{ route('view_listAgency_in_province', [$item->slug, $item->id]) }}">{{ $item->name }}</a></li>
                                @endif
                            @endforeach
                        </li>
                        <li class="list-group-item active sub1" >Miền Trung
                            @foreach ($listProvince as $item)
                                @if ($item->region == 2)
                                <li class="list-group-item sub2-daily" data-id="{{ $item->id }}" ><a href="{{ route('view_listAgency_in_province', [$item->slug, $item->id]) }}">{{ $item->name }}</a></li>
                                @endif
                            @endforeach
                        </li>
                        <li class="list-group-item active sub1" >Miền Nam
                            @foreach ($listProvince as $item)
                                @if ($item->region == 3)
                                <li class="list-group-item sub2-daily" data-id="{{ $item->id }}" ><a href="{{ route('view_listAgency_in_province', [$item->slug, $item->id]) }}">{{ $item->name }}</a></li>
                                @endif
                            @endforeach
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="row">
                    @foreach ($listAgency as $item)
                        <div class="col-12">
                            <div class="card t-card-daily mb-3">
                                <div class="row no-gutters">
                                    <div class="col-md-4 t-bao-anhbia">
                                    <a href="{{ route('view_detailAgency', [$item->slug, $item->id]) }}"><img class="card-img" style="max-width: 100%;" src="{{ $item->image }}" alt="{{ $item->name }}"></a>
                                    </div>
                                    <div class="col-md-4 d-flex">
                                        <div class="card-body ">
                                            <a href="{{ route('view_detailAgency', [$item->slug, $item->id]) }}"><h5 class="card-title">{{ $item->name }}</h5></a>
                                            <p class="card-text">Địa chỉ: {{ $item->address }}</p>
                                            <p class="card-text">Hotline: {{ $item->phone }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 t-bao-iframe-bando">
                                    <iframe src="https://www.google.com/maps/embed?{{ $item->map }}" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
                {!! $listAgency->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        let activeLeft = $('#activeLeft').val();
    $('.sub2-daily').each(function(){
        let id =$(this).data('id');
        if(id == activeLeft){
            $(this).addClass('actives__')
        }
    })
    </script>
@endpush

