@extends('client.layout.index')
@section('title', $agency->name )
@section('description', $agency->description )
@section('keywords', $agency->keyword )
@section('content')
<div class="container-fluid mb-2">
    <div class="container">
        <nav aria-label="breadcrumb" class="mt-3">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Trang chủ</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $agency->name }}</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-12 col-md-5 t-bao-albumdaily">
                <ul class="t-album-anh-daily">
                    @foreach (json_decode($agency->album) as $item)
                        <li data-thumb="{{ $item }}">
                            <img width="100%" src="{{ $item }}" />
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-md-7">
                <h1>{{ $agency->name }}</h1>
                <p>Địa chỉ: {{ $agency->address }}</p>
                <div class="socials-share">
                    <a class="bg-facebook" href="{{ $agency->facebook }}" target="_blank"><span class="fa fa-facebook"></span> Fanpage</a>
                    <a class="bg-google-plus" href="tel:{{ $agency->phone }}" target="_blank"><span class="fa fa-phone"></span> Hotline : {{ $agency->phone }}</a>
                    <a class="bg-twitter" href="https://zalo.me/{{ $agency->zalo }}" target="_blank"> <span class="fa fa-mobile"></span> Zalo</a>
                </div>
            </div>
            <div class="col-12 col-md-12 mt-4">
                {!! $agency->content !!}
                <iframe src="https://www.google.com/maps/embed?{{ $agency->map }}" width="100%" height="400px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</div>
@endsection


