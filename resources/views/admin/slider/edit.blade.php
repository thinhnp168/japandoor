@extends('admin.layout.index')
@section('tilte_site', 'Chỉnh sửa slider')
@section('content')

<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Chỉnh sửa slider</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên slider</label>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên slider" class="form-control @error('name') is-invalid @enderror" value="{{ $slider->name }}">
                    
                </div>
                <div class="form-group">
                    <label for="nf-link" class=" form-control-label">Link</label>
                    <input type="text" id="nf-link" name="link" placeholder="Link" class="form-control @error('link') is-invalid @enderror" value="{{ $slider->link }}">
                    
                </div>
                
                <div class="form-group">
                    <label for="nf-status" class=" form-control-label">Xuất bản</label>
                    <select name="status" id="nf-status" class="form-control">
                        @if ($slider->status === 0)
                            <option value="0">Ẩn</option>
                            <option value="1">Hiện</option>
                        @else
                            <option value="1">Hiện</option>
                            <option value="0">Ẩn</option>
                        @endif
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="nf-image" class=" form-control-label">Ảnh slider</label><span class="text-danger font-weight-bold">*</span>
                    @error('image')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                    <div class="card-box text-center d-flex flex-column align-items-center">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-modal-image">Select image <span class="text-danger">*</span></button>
                            <input type="hidden" name="image" id="image" value="{{ $slider->image }}">
                        </span>
                        <div id="preview_image" class="mt-3 preview_image">
                            <div class="box_imgg position-relative">
                                <img src="{{ str_replace('/source/', '/thumbs/', $slider->image) }}" id='show-img-image' class="show-img-image" style="width:100%;">
                                <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Lưu lại
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
<div class="modal fade bd-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Manger Image</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0"
                    src="{{ url('/') }}/jadmin/responsive_filemanager/filemanager/dialog.php?type=0&field_id=image">
                </iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

 <script>
        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#image').val('');
            });
        });
        function responsive_filemanager_callback(field_id){
            var _img = $('input#' + field_id).val();
            console.log(_img);
            if (!_img.length) {
                $('#preview_image').empty();
            } else {
                $('#preview_image').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-image' class="show-img-image" style="width:100%;">
                        <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                    </div>
                `;
                $('#preview_image').append($html);
                let __img = _img.replace("/source/", "/thumbs/");
                $('#show-img-image').attr('src', __img);
            }
        }
 </script>
@endpush