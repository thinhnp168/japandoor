
@extends('admin.layout.index')
@section('tilte_site', 'Danh sách phụ kiện')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Danh sách phụ kiện</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Ảnh đại diện</th>
                                <th>Mã phụ kiện</th>
                                <th>Giá </th>
                                <th>Xuất bản</th>
                                <th>Nổi bật</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listAccessory as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <img src="{{ str_replace('/source/', '/thumbs/', $item->image) }}" alt="" style='with:100%'>
                                    </td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->price }}</td>
                                    @if($item->status == 1)
                                        <td class="midle-center"><input type="checkbox" checked="checked" class="checkbox_status change_info" data-type="status" data-id="{{ $item->id }}"></td>
                                    @else
                                        <td class="midle-center"> <input type="checkbox" class="checkbox_status change_info" data-type="status" data-id="{{ $item->id }}"></td>
                                    @endif
                                    @if($item->highlight == 1)
                                        <td class="midle-center"><input type="checkbox" checked="checked" class="checkbox_status change_info" data-type="highlight" data-id="{{ $item->id }}"></td>
                                    @else
                                        <td class="midle-center"> <input type="checkbox" class="checkbox_status change_info" data-type="highlight" data-id="{{ $item->id }}"></td>
                                    @endif
                                   
                                   
                                    <td>
                                        <a href="{{ route('formEditAccessory', $item->id) }}" class="badge badge-primary">Sửa</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('deleteAccessory', $item->id) }}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).on('change','.change_info', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                type : type
            }
            let url_ = @json(route('changeInfoAccessory'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else {
                toastr.warning(msg.mess);
                $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
    </script>
@endpush