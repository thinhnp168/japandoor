<!doctype html>

<html class="no-js" lang="vi">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('tilte_site')</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="__token" content="{{csrf_token()}}">
    
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="icon" type="image/png" sizes="16x16"  href="/client/asset/images/favicon-16x16.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="/jadmin/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/jadmin/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/jadmin/vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="/jadmin/vendors/toastr/toastr.min.css">


    <link rel="stylesheet" href="/jadmin/assets/css/style.css">
    <link rel="stylesheet" href="/jadmin/assets/css/admin_t.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>


    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/admin"><img src="/jadmin/images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="/admin"><img src="/jadmin/images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">Admin</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>User</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddUser') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listUsers') }}">Danh sách</a></li>

                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Danh mục bài viết</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddCategoryPost') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listCategoryPost') }}">Danh sách</a></li>

                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý bài viết</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddPost') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listPost') }}">Danh sách</a></li>

                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Bộ lọc màu sắc</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddColor') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listColor') }}">Danh sách</a></li>

                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý phụ kiện</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddAccessory') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listAccessory') }}">Danh sách</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý slider</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddSlider') }}">Thêm mới</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listSlider') }}">Danh sách</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý Đại lý</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddProvince') }}">Thêm mới tỉnh, thành</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listProvince') }}">Danh sách tỉnh, thành</a></li>
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddAgency') }}">Thêm mới đại lý</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listAgency') }}">Danh sách đại lý</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Danh mục sản phẩm</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddCategory') }}">Thêm mới danh mục</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listCategory') }}">Danh sách danh mục</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user"></i>Quản lý sản phẩm</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-plus"></i><a href="{{ route('formAddProduct') }}">Thêm mới sản phẩm</a></li>
                            <li><i class="fa fa-list"></i><a href="{{ route('listProduct') }}">Danh sách sản phẩm</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ route('listEmail') }}" class="dropdown-toggle" > <i class="menu-icon fa fa-user"></i>Email theo dõi</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ route('listContact') }}" class="dropdown-toggle" > <i class="menu-icon fa fa-user"></i>Thông tin liên hệ</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ route('formEditSetting') }}" class="dropdown-toggle" > <i class="menu-icon fa fa-user"></i>Cấu hình website</a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="{{ route('listOrder') }}" class="dropdown-toggle" > <i class="menu-icon fa fa-user"></i>Thông tin đặt hàng</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <header id="header" class="header">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <span class="count bg-danger">5</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="notification">
                                <p class="red">You have 3 Notification</p>
                                <a class="dropdown-item media bg-flat-color-1" href="#">
                                    <i class="fa fa-check"></i>
                                    <p>Server #1 overloaded.</p>
                                </a>
                                <a class="dropdown-item media bg-flat-color-4" href="#">
                                    <i class="fa fa-info"></i>
                                    <p>Server #2 overloaded.</p>
                                </a>
                                <a class="dropdown-item media bg-flat-color-5" href="#">
                                    <i class="fa fa-warning"></i>
                                    <p>Server #3 overloaded.</p>
                                </a>
                            </div>
                        </div>

                        <div class="dropdown for-message">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ti-email"></i>
                                <span class="count bg-primary">9</span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="message">
                                <p class="red">You have 4 Mails</p>
                                <a class="dropdown-item media bg-flat-color-1" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="/jadmin/images/avatar/1.jpg"></span>
                                    <span class="message media-body">
                                        <span class="name float-left">Jonathan Smith</span>
                                        <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                    </span>
                                </a>
                                <a class="dropdown-item media bg-flat-color-4" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="/jadmin/images/avatar/2.jpg"></span>
                                    <span class="message media-body">
                                        <span class="name float-left">Jack Sanders</span>
                                        <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </span>
                                </a>
                                <a class="dropdown-item media bg-flat-color-5" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="/jadmin/images/avatar/3.jpg"></span>
                                    <span class="message media-body">
                                        <span class="name float-left">Cheryl Wheeler</span>
                                        <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                    </span>
                                </a>
                                <a class="dropdown-item media bg-flat-color-3" href="#">
                                    <span class="photo media-left"><img alt="avatar" src="/jadmin/images/avatar/4.jpg"></span>
                                    <span class="message media-body">
                                        <span class="name float-left">Rachel Santos</span>
                                        <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="/jadmin/images/admin.jpg" alt="User Avatar">
                        </a>
                        @if (Auth::check())
                            <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa-user"></i> My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa-user"></i> Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa-cog"></i> Settings</a>

                                <a class="nav-link" href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                            </div>
                        @else
                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="#"><i class="fa fa-user"></i> My Profile</a>
                           
                        </div>
                        @endif
                        
                    </div>

                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language">
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->

        {{-- <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div> --}}

        <div class="content mt-3">

            @yield('content')


        </div> <!-- .content -->
    </div><!-- /#right-panel -->

    <!-- Right Panel -->
    <script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script src="/jadmin/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/jadmin/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="/jadmin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/jadmin/assets/js/main.js"></script>


    {{-- <script src="/jadmin/vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="/jadmin/assets/js/dashboard.js"></script>
    <script src="/jadmin/assets/js/widgets.js"></script>
    <script src="/jadmin/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="/jadmin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="/jadmin/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script> --}}

    <script src="/jadmin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/jadmin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/jadmin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/jadmin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="/jadmin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/jadmin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/jadmin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="/jadmin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/jadmin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/jadmin/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="/jadmin/assets/js/init-scripts/data-table/datatables-init.js"></script>
    <script src="/jadmin/vendors/toastr/toastr.min.js"></script>
    {{-- <script>
        (function($) {
            "use strict";

            jQuery('#vmap').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#1de9b6', '#03a9f5'],
                normalizeFunction: 'polynomial'
            });
        })(jQuery);
    </script> --}}
    @if(session()->has('success'))
        <script>
            let mess = '{{ session()->get('success') }}';
            toastr.success(mess);
        </script>
    @endif
    
    @stack('scripts')

</body>

</html>