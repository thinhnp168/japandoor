
@extends('admin.layout.index')
@section('tilte_site', 'Email đăng ký')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Email đăng ký</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="midle-center">ID</th>
                                <th class="midle-center">Email</th>
                                <th class="midle-center">Thời gian</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listEmail as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td class="midle-center">{{ $item->email }}</td>
                                    <td class="midle-center">{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
