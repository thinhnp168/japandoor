
@extends('admin.layout.index')
@section('tilte_site', 'Danh sách tỉnh, thành phố có đại lý')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Danh sách tỉnh, thành phố có đại lý</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Slug</th>
                                <th>Khu vực</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listProvince as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td>
                                        <input type="text" class="form-control change-value" value="{{ $item->name }}" data-id="{{ $item->id }}" data-type="name">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control change-value" value="{{ $item->slug }}" data-id="{{ $item->id }}" data-type="slug">
                                    </td>
                                    <td>
                                        <select name=""  class="form-control change-value" data-id="{{ $item->id }}" data-type="region">
                                            <option value="1" @if ($item->region == 1) selected @endif>Miền Bắc</option>
                                            <option value="2" @if ($item->region == 2) selected @endif>Miền Trung</option>
                                            <option value="3" @if ($item->region == 3) selected @endif>Miền Nam</option>
                                        </select>
                                    </td>
                                    <td>
                                        <a href="{{ route('deleteProvince', $item->id) }}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).on('change','.change-value', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let value = $(__this).val();
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                value : value,
                type : type
            }
            let url_ = @json(route('changeValueProvince'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else if (msg.type == 2) {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                } else if (msg.type == 3) {
                    toastr.success(msg.mess);
                    $(__this).val(msg.value);
                    $(__this).prop('disabled', false);
                } 
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
        
    </script>
@endpush