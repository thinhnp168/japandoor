@extends('admin.layout.index')
@section('tilte_site', 'Thêm mới đại lý')
@section('content')
<script src='/jadmin/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    	
    tinymce.init({
        selector: '.nf-content',
        theme: 'modern',
        mode : "textareas",
        width: '100%',
        height: 300,
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor qrcode youtube responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons youtube qrcode  responsivefilemanager fontsizeselect',
        external_filemanager_path:"/jadmin/responsive_filemanager/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/jadmin/responsive_filemanager/filemanager/plugin.min.js"}
    });
    </script>
<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Thêm mới đại lý</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên đại lý</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên đại lý" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                    @error('name')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-address" class=" form-control-label">Địa chỉ</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-address" name="address" placeholder="Nhập địa chỉ" class="form-control @error('address') is-invalid @enderror" value="{{ old('address') }}">
                    @error('address')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-phone" class=" form-control-label">Số điện thoại</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-phone" name="phone" placeholder="Nhập số điện thoại" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}">
                    @error('phone')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-zalo" class=" form-control-label">Zalo</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-zalo" name="zalo" placeholder="Nhập zalo" class="form-control @error('zalo') is-invalid @enderror" value="{{ old('zalo') }}">
                    @error('zalo')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-facebook" class=" form-control-label">Facebook</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-facebook" name="facebook" placeholder="Nhập facebook" class="form-control @error('facebook') is-invalid @enderror" value="{{ old('facebook') }}">
                    @error('facebook')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-map" class=" form-control-label">Bản đồ </label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-map" name="map" placeholder="Nhập id map" class="form-control @error('map') is-invalid @enderror" value="{{ old('map') }}">
                    @error('map')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-status" class=" form-control-label">Xuất bản</label><span class="text-danger font-weight-bold">*</span>
                    <select name="status" id="nf-status" class="form-control">
                        @if (old('status') === 0)
                            <option value="0">Ẩn</option>
                            <option value="1">Hiện</option>
                        @else
                            <option value="1">Hiện</option>
                            <option value="0">Ẩn</option>
                        @endif
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="nf-province_id" class=" form-control-label">Tỉnh, Thành phố</label><span class="text-danger font-weight-bold">*</span>
                    <select name="province_id" id="nf-province_id" class="form-control">
                    @foreach ($province as $item)
                        <option value="{{ $item->id }}" @if ($item->id == old('province_id')) selected @endif>{{ $item->name }}</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nf-image" class=" form-control-label">Ảnh đại diện</label><span class="text-danger font-weight-bold">*</span>
                    @error('image')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                    <div class="card-box text-center d-flex flex-column align-items-center">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-modal-image">Select image <span class="text-danger">*</span></button>
                            <input type="hidden" name="image" id="image" value="{{ old('image') }}">
                            
                            @if ($errors->has('image'))
                            <p class="help is-danger">{{ $message }}</p>
                            @endif
                        </span>
                        <div id="preview_image" class="mt-3 preview_image">
                            @if (!empty(old('image')))
                            <div class="box_imgg position-relative">
                                <img src="{!! str_replace('/source/','/thumbs/', old('image')) !!}" id='show-img-image' class="show-img-image" style="width:100%;">
                                <i class="fa fa-times-circle style_icon_remove style_icon_remove_image" title="delete"></i>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-title" class=" form-control-label">Title</label>
                    <input type="text" id="nf-title" name="title" placeholder="Title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}">
                    @error('title')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-description" class=" form-control-label">Description</label>
                    <textarea name="description" id="nf-description" rows="3" placeholder="Description" class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                    @error('description')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-keyword" class=" form-control-label">Keyword</label>
                    <textarea name="keyword" id="nf-keyword" rows="3" placeholder="Keyword" class="form-control @error('keyword') is-invalid @enderror">{{ old('keyword') }}</textarea>
                    @error('keyword')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="banner">Album ảnh<span class="text-danger">*</span></label>
                    <div class="row">
                        <div class="col-3">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-album">Select album</button>
                                <input type="hidden" name="album" id="album" value="{{ old('album') }}" >
                            </span>
                        </div>
                    </div>
                    <div id="preview_album" class="mt-3">
                        <div class="row row_preview_album" id="row_preview_album">
                            @if ($errors->has('album'))
                                <p class="help is-danger">{{ $errors->first('album') }}</p>
                            @endif
                            @if (!empty( old('album')))
                                @foreach (json_decode( old('album')) as $item)
                                    <div class="col-3 mt-3">
                                        <div class="box_imgg position-relative">
                                            <img src="{{ $item }}" class="img-height-110" style="width:100%; height=110px;">
                                            <i class="fa fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nf-content" class=" form-control-label">Content</label>
                    <textarea name="content" id="nf-content" rows="3" placeholder="Content" class="nf-content form-control @error('content') is-invalid @enderror">{{ old('content') }}</textarea>
                    @error('content')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Thêm
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
<div class="modal fade bd-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Manger Image</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0"
                    src="{{ url('/') }}/jadmin/responsive_filemanager/filemanager/dialog.php?type=0&field_id=image">
                </iframe>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-album" id="modal-file-album" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Manger Image</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0"
                    src="{{ url('/') }}/jadmin/responsive_filemanager/filemanager/dialog.php?type=0&field_id=album&multiple=1">
                </iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

 <script>
        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#image').val('');
            });
        });
        
        function responsive_filemanager_callback(field_id){
            var _img = $('input#' + field_id).val();
            if (field_id == 'image') {
                if (!_img.length) {
                    $('#preview_image').empty();
                } else {
                    $('#preview_image').empty();
                    $html = `
                        <div class="box_imgg position-relative">
                            <img src="" id='show-img-image' class="show-img-image" style="width:100%;">
                            <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                        </div>
                    `;
                    $('#preview_image').append($html);
                    let __img = _img.replace("/source/", "/thumbs/");
                    $('#show-img-image').attr('src', __img);
                }
            } else if(field_id == 'album') {
                if (!_img.length) {
                    $('#preview_album').empty();
                } else {
                    if(_img[0] == '[') {
                        var array = JSON.parse(_img);
                        $('#row_preview_album').empty();
                        var html = '';
                        $.each(array, function( index, value ) {
                            html += `
                                <div class="col-3 mt-3">
                                    <div class="box_imgg position-relative">
                                        <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                        <i class="fa fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                    </div>
                                </div>
                            `;
                        });
                        $('#row_preview_album').append(html);

                    }else{

                        $html = `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="" id='show-img-album' style="width:100%;">
                                    <i class="fa fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                        $('#row_preview_album').empty();
                        $('#row_preview_album').append($html);
                        $('#show-img-album').attr('src', _img);
                        var str_src = '';
                        str_src += "[\"";
                        str_src += _img;
                        str_src += "\"]";
                        $('#album').val(str_src);
                    }
                }
            }
        }
        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
 </script>
@endpush