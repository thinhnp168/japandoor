@extends('admin.layout.index')
@section('tilte_site', 'Thêm mới tỉnh, thành phố có đại lý')
@section('content')

<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Thêm mới tỉnh, thành phố có đại lý</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12 ">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên tỉnh, thành phố</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên tỉnh, thành phố" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                    @error('name')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-region" class=" form-control-label">Khu vực</label><span class="text-danger font-weight-bold">*</span>
                    <select name="region" id="nf-region" class="form-control">
                        @if (old('region') === 1)
                            <option value="1" selected>Miền Bắc</option>
                            <option value="2">Miền Trung</option>
                            <option value="3">Miền Nam</option>
                        @elseif(old('region') === 2)
                            <option value="1" >Miền Bắc</option>
                            <option value="2" selected>Miền Trung</option>
                            <option value="3">Miền Nam</option>
                        @else
                            <option value="1" >Miền Bắc</option>
                            <option value="2" >Miền Trung</option>
                            <option value="3" selected>Miền Nam</option>
                        @endif
                        
                    </select>
                </div>
               
            </div>
            
        </div>
        
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Thêm
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>

@endsection
