@extends('admin.layout.index')
@section('tilte_site', 'Thêm mới màu sắc')
@section('content')
<script src='/jadmin/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    	
    tinymce.init({
        selector: '.nf-content',
        theme: 'modern',
        mode : "textareas",
        width: '100%',
        height: 300,
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor qrcode youtube responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons youtube qrcode  responsivefilemanager fontsizeselect',
        external_filemanager_path:"/jadmin/responsive_filemanager/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/jadmin/responsive_filemanager/filemanager/plugin.min.js"}
    });
    </script>
<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Thêm mới màu sắc</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên </label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                    @error('name')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="nf-status" class=" form-control-label">Xuất bản</label><span class="text-danger font-weight-bold">*</span>
                    <select name="status" id="nf-status" class="form-control">
                        @if (old('status') === 0)
                            <option value="0">Ẩn</option>
                            <option value="1">Hiện</option>
                        @else
                            <option value="1">Hiện</option>
                            <option value="0">Ẩn</option>
                        @endif
                        
                    </select>
                </div>
            </div>
        </div>   
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Thêm
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
@endsection
