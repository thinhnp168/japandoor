@extends('admin.layout.index')
@section('tilte_site', 'Chỉnh sửa bài viết')
@section('content')
<script src='/jadmin/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    	
    tinymce.init({
        selector: '.nf-content',
        theme: 'modern',
        mode : "textareas",
        width: '100%',
        height: 300,
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'save table contextmenu directionality emoticons template paste textcolor qrcode youtube responsivefilemanager'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons youtube qrcode  responsivefilemanager fontsizeselect',
        external_filemanager_path:"/jadmin/responsive_filemanager/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/jadmin/responsive_filemanager/filemanager/plugin.min.js"}
    });
    </script>
<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Chỉnh sửa bài viết</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên bài viết</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên bài viết" class="form-control @error('name') is-invalid @enderror" value="{{ $post->name }}">
                    @error('name')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-slug" class=" form-control-label">Tên bài viết</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-slug" name="slug" placeholder="Nhập tên bài viết" class="form-control @error('slug') is-invalid @enderror" value="{{ $post->slug }}">
                    @error('slug')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-summary" class=" form-control-label">Mô tả ngắn</label><span class="text-danger font-weight-bold">*</span>
                    <textarea name="summary" id="nf-summary" rows="3" placeholder="Mô tả ngắn" class="form-control @error('summary') is-invalid @enderror">{{ $post->summary }}</textarea>
                </div>
                <div class="form-group">
                    <label for="nf-status" class=" form-control-label">Xuất bản</label><span class="text-danger font-weight-bold">*</span>
                    <select name="status" id="nf-status" class="form-control">
                        @if ($post->status === 0)
                            <option value="0">Ẩn</option>
                            <option value="1">Hiện</option>
                        @else
                            <option value="1">Hiện</option>
                            <option value="0">Ẩn</option>
                        @endif
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="nf-highlight" class=" form-control-label">Nổi bật</label><span class="text-danger font-weight-bold">*</span>
                    <select name="highlight" id="nf-highlight" class="form-control">
                        @if ($post->highlight === 1)
                            <option value="1">Có</option>
                            <option value="0">Không</option>
                        @else
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        @endif
                        
                    </select>
                </div>
                <div class="form-group">
                    <label for="nf-image" class=" form-control-label">Ảnh đại diện</label><span class="text-danger font-weight-bold">*</span>
                    @error('image')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                    <div class="card-box text-center d-flex flex-column align-items-center">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-modal-image">Select image <span class="text-danger">*</span></button>
                            <input type="hidden" name="image" id="image" value="{{ $post->image }}">
                        </span>
                        <div id="preview_image" class="mt-3 preview_image">
                            <div class="box_imgg position-relative">
                                <img src="{{ str_replace('/source/', '/thumbs/', $post->image) }}" id='show-img-image' class="show-img-image" style="width:100%;">
                                <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nf-category" class=" form-control-label">Danh mục</label><span class="text-danger font-weight-bold">*</span> <br>
                    <select name="category_id" id="nf-category" class="form-control">
                        @foreach ($category as $item)
                            <option @if ($item->id == $post->category_id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach 
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-title" class=" form-control-label">Title</label>
                    <input type="text" id="nf-title" name="title" placeholder="Title" class="form-control @error('title') is-invalid @enderror" value="{{ $post->title }}">
                    
                </div>
                <div class="form-group">
                    <label for="nf-description" class=" form-control-label">Description</label>
                    <textarea name="description" id="nf-description" rows="3" placeholder="Description" class="form-control @error('description') is-invalid @enderror">{{ $post->description }}</textarea>
                    
                </div>
                <div class="form-group">
                    <label for="nf-keyword" class=" form-control-label">Keyword</label>
                    <textarea name="keyword" id="nf-keyword" rows="3" placeholder="Keyword" class="form-control @error('keyword') is-invalid @enderror">{{ $post->keyword }}</textarea>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="nf-content" class=" form-control-label">Content</label>
                    <textarea name="content" id="nf-content" rows="3" placeholder="Content" class="nf-content form-control @error('content') is-invalid @enderror">{{ $post->content }}</textarea>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Lưu lại
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
<div class="modal fade bd-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Manger Image</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0"
                    src="{{ url('/') }}/jadmin/responsive_filemanager/filemanager/dialog.php?type=0&field_id=image">
                </iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

 <script>
        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#image').val('');
            });
        });
        function responsive_filemanager_callback(field_id){
            var _img = $('input#' + field_id).val();
            console.log(_img);
            if (!_img.length) {
                $('#preview_image').empty();
            } else {
                $('#preview_image').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-image' class="show-img-image" style="width:100%;">
                        <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                    </div>
                `;
                $('#preview_image').append($html);
                let __img = _img.replace("/source/", "/thumbs/");
                $('#show-img-image').attr('src', __img);
            }
        }
 </script>
@endpush