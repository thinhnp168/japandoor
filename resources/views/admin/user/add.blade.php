@extends('admin.layout.index')
@section('tilte_site', 'Thêm mới Người dùng')
@section('content')
<div class="card">
    <div class="card-header">
        <strong>Thêm mới người dùng</strong>
    </div>
<form action="" method="post" class="">
    @csrf 
    <div class="card-body card-block">
        
            <div class="form-group">
                <label for="nf-ten" class=" form-control-label">Họ và tên</label><span class="text-danger font-weight-bold">*</span>
                <input type="text" id="nf-ten" name="ten" placeholder="Nhập tên người dùng" class="form-control @error('ten') is-invalid @enderror" value="{{ old('ten') }}">
                @error('ten')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nf-email" class=" form-control-label">Tài khoản</label><span class="text-danger font-weight-bold">*</span>
                <input type="text" id="nf-email" name="email" placeholder="Nhập email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                @error('email')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nf-dienthoai" class=" form-control-label">Điện thoại</label><span class="text-danger font-weight-bold">*</span>
                <input type="text" id="nf-dienthoai" name="dienthoai" placeholder="Nhập số điện thoại" class="form-control @error('dienthoai') is-invalid @enderror" value="{{ old('dienthoai') }}">
                @error('dienthoai')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nf-password" class=" form-control-label">Password</label><span class="text-danger font-weight-bold">*</span>
                <input type="password" id="nf-password" name="password" placeholder="Nhập mật khẩu" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="" class=" form-control-label">Xuất bản</label>
                <div class="form-check pl-4">
                    <div class="radio">
                        <label for="radio1" class="form-check-label ">
                            <input type="radio" id="radio1" name="xuatban" value="1" class="form-check-input" checked>Xuất bản
                        </label>
                    </div>
                    <div class="radio">
                        <label for="radio2" class="form-check-label ">
                            <input type="radio" id="radio2" name="xuatban" value="2" class="form-check-input">Khoá
                        </label>
                    </div>
                </div>
            </div>

        
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Thêm
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>

@endsection
