@extends('admin.layout.index')
@section('tilte_site', 'Danh sách Người dùng')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Data Table</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Email</th>
                                <th>Điện thoại</th>
                                <th>Đổi mật khẩu</th>
                                <th>Xuất bản</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listUser as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td>{{ $item->ten }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->dienthoai }}</td>
                                    <td class="position-relative change_password_td">
                                        <input type="password" autocomplete="off" class="w-100 change_password" data-id="{{ $item->id }}" name="password" placeholder="Thay đổi mật khẩu" class="form-control @error('password') is-invalid @enderror">
                                        <i class="position-absolute hide_unhide_pass fa fa-eye"></i>
                                    </td>
                                    @if($item->xuatban == 1)
                                        <td class="midle-center"><input type="checkbox" checked="checked" class="checkbox_user" data-type="status" data-id="{{ $item->id }}"></td>
                                    @else
                                        <td class="midle-center"> <input type="checkbox" class="checkbox_user" data-type="status" data-id="{{ $item->id }}"></td>
                                    @endif
                                    <td>
                                        <a href="{{ route('formEditUser', $item->id) }}" class="badge badge-primary">Sửa</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('deleteUser', $item->id) }}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).on('change','.checkbox_user', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id
            }
            let url_ = @json(route('changeInfoUser'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else {
                toastr.warning(msg.mess);
                $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
        $(document).on('change', '.change_password', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let password = $(__this).val();
            if (password.length < 4) {
                toastr.error("Mật khẩu không được ít hơn 4 ký tự");
                $(__this).prop('disabled', false);
                return false;
            }
            if (password.length > 31) {
                toastr.error("Mật khẩu không được nhiều hơn 31 ký tự");
                $(__this).prop('disabled', false);
                return false;
            }
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                password : password
            }
            let url_ = @json(route('changePassUser'));
            let url = url_ + '/' + id;
            let request = $.ajax({
                url: url,
                type: "POST",
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else {
                toastr.warning(msg.mess);
                $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
        $(document).on('click', '.hide_unhide_pass' , function (e) {
            e.preventDefault();
            const __this = this;
            $(__this).prop('disabled', true);
            if($(__this).parent('.change_password_td').children('.change_password').attr('type') == 'password') {
                $(__this).parent('.change_password_td').children('.change_password').attr('type', 'text');
                $(__this).removeClass('fa-eye');
                $(__this).addClass('fa-eye-slash');
                $(__this).prop('disabled', false);
                return false;
            }else {
                $(__this).parent('.change_password_td').children('.change_password').attr('type', 'password');
                $(__this).addClass('fa-eye');
                $(__this).removeClass('fa-eye-slash');
                $(__this).prop('disabled', false);
                return false;
            }
        })
    </script>
@endpush