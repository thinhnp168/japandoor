
@extends('admin.layout.index')
@section('tilte_site', 'Thông tin đặt hàng')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Thông tin đặt hàng</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="midle-center">ID</th>
                                <th class="midle-center">Họ tên</th>
                                <th class="midle-center">Số điện thoại</th>
                                <th class="midle-center">Tỉnh, Thành phố</th>
                                <th class="midle-center">Thời gian thực hiện</th>
                                <th class="midle-center">Số lượng cửa</th>
                                <th class="midle-center">Số lượng cửa sổ</th>
                                <th class="midle-center">Thông tin khác</th>
                                <th class="midle-center">Thời gian</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listOrder as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td class="midle-center">{{ $item->name }}</td>
                                    <td class="midle-center">{{ $item->phone }}</td>
                                    <td class="midle-center">{{ $item->province }}</td>
                                    <td class="midle-center">{{ $item->month }}</td>
                                    <td class="midle-center">{{ $item->door }}</td>
                                    <td class="midle-center">{{ $item->window }}</td>
                                    <td class="midle-center">{{ $item->content }}</td>
                                    <td class="midle-center">{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
