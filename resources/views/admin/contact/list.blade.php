
@extends('admin.layout.index')
@section('tilte_site', 'Thông tin liên hệ')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Thông tin liên hệ</strong>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="midle-center">ID</th>
                                <th class="midle-center">Tiêu đề</th>
                                <th class="midle-center">Họ tên</th>
                                <th class="midle-center">Số điện thoại</th>
                                <th class="midle-center">Email</th>
                                <th class="midle-center">Địa chỉ</th>
                                <th class="midle-center">Ý kiến</th>
                                <th class="midle-center">Thời gian</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listContact as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td class="midle-center">{{ $item->title }}</td>
                                    <td class="midle-center">{{ $item->name }}</td>
                                    <td class="midle-center">{{ $item->phone }}</td>
                                    <td class="midle-center">{{ $item->email }}</td>
                                    <td class="midle-center">{{ $item->address }}</td>
                                    <td class="midle-center">{{ $item->content }}</td>
                                    <td class="midle-center">{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
