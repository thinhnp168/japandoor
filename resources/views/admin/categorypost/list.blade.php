
@extends('admin.layout.index')
@section('tilte_site', 'Danh sách danh mục tin')
@section('content')

<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <strong class="card-title">Danh sách danh mục tin</strong>
                    <a href="{{ route('formAddCategoryPost') }}" class="btn btn-primary rounded "><i class="fa fa-plus"></i> Thêm mới</a>
                </div>
                <div class="card-body table-responsive">
                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Loại danh mục</th>
                                <th>Xuất bản</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($listCategoryPost as $item)
                                <tr>
                                    <td class="midle-center">{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                   
                                    @if($item->type == 1)
                                        <td class="midle-center">
                                            <select class="select_type form-control change_info" data-id="{{ $item->id }}" data-type="type">
                                                <option value="1" selected>Danh mục nhiều tin</option>
                                                <option value="0">Danh mục tin đơn</option>
                                            </select>    
                                        </td>
                                    @else
                                    <td class="midle-center">
                                        <select class="select_type form-control change_info" data-id="{{ $item->id }}" data-type="type">
                                            <option value="1">Danh mục nhiều tin</option>
                                            <option value="0" selected>Danh mục tin đơn</option>
                                        </select>    
                                    </td>
                                    @endif
                                    @if($item->status == 1)
                                        <td class="midle-center"><input type="checkbox" checked="checked" class="checkbox_status change_info" data-type="status" data-id="{{ $item->id }}"></td>
                                    @else
                                        <td class="midle-center"> <input type="checkbox" class="checkbox_status change_info" data-type="status" data-id="{{ $item->id }}"></td>
                                    @endif
                                   
                                   
                                    <td>
                                        <a href="{{ route('formEditCategoryPost', $item->id) }}" class="badge badge-primary">Sửa</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('deleteCategoryPost', $item->id) }}" class="badge badge-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).on('change','.change_info', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                type : type
            }
            let url_ = @json(route('changeInfoCategoryPost'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else {
                toastr.warning(msg.mess);
                $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
        })
        // $(document).on('change', '.change_password', function (e) {
        //     e.preventDefault();
        //     toastr.clear();
        //     toastr.options = {
        //         "closeButton": true,
        //         "timeOut": "5000",
        //         "positionClass": "toast-top-right"
        //     }
        //     const __this = this;
        //     $(__this).prop('disabled', true);
        //     let id = $(__this).attr('data-id');
        //     let password = $(__this).val();
        //     if (password.length < 4) {
        //         toastr.error("Mật khẩu không được ít hơn 4 ký tự");
        //         $(__this).prop('disabled', false);
        //         return false;
        //     }
        //     if (password.length > 31) {
        //         toastr.error("Mật khẩu không được nhiều hơn 31 ký tự");
        //         $(__this).prop('disabled', false);
        //         return false;
        //     }
        //     const __token = $('meta[name="__token"]').attr('content');
        //     data_ = {
        //         _token: __token,
        //         id : id,
        //         password : password
        //     }
        //     let url_ = @json(route('changePassUser'));
        //     let url = url_ + '/' + id;
        //     let request = $.ajax({
        //         url: url,
        //         type: "POST",
        //         data: data_,
        //         dataType: "json"
        //     });
        //     request.done(function (msg) {
        //         if (msg.type == 1) {
        //         toastr.success(msg.mess);
        //         $(__this).prop('disabled', false);
        //         }else {
        //         toastr.warning(msg.mess);
        //         $(__this).prop('disabled', false);
        //         }
        //         return false;
        //     });

        //     request.fail(function (jqXHR, textStatus) {
        //         alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
        //     });
        // })
        // $(document).on('click', '.hide_unhide_pass' , function (e) {
        //     e.preventDefault();
        //     const __this = this;
        //     $(__this).prop('disabled', true);
        //     if($(__this).parent('.change_password_td').children('.change_password').attr('type') == 'password') {
        //         $(__this).parent('.change_password_td').children('.change_password').attr('type', 'text');
        //         $(__this).removeClass('fa-eye');
        //         $(__this).addClass('fa-eye-slash');
        //         $(__this).prop('disabled', false);
        //         return false;
        //     }else {
        //         $(__this).parent('.change_password_td').children('.change_password').attr('type', 'password');
        //         $(__this).addClass('fa-eye');
        //         $(__this).removeClass('fa-eye-slash');
        //         $(__this).prop('disabled', false);
        //         return false;
        //     }
        // })
    </script>
@endpush