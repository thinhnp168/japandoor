@extends('admin.layout.index')
@section('tilte_site', 'Cấu hình website')
@section('content')

<div class="card">
    <form action="" method="post" class="">
        @csrf 
    <div class="card-header">
        <strong>Cấu hình website</strong>
    </div>

    <div class="card-body card-block">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-name" class=" form-control-label">Tên công ty</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-name" name="name" placeholder="Nhập tên công ty" class="form-control @error('name') is-invalid @enderror" value="{{ $editSetting->name }}">
                    @error('name')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-email" class=" form-control-label">Email mặc định</label><span class="text-danger font-weight-bold">*</span>
                    <input type="email" id="nf-email" name="email" placeholder="Nhập email" class="form-control @error('email') is-invalid @enderror" value="{{ $editSetting->email }}">
                    @error('email')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-phone" class=" form-control-label">Số điện thoại mặc định</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-phone" name="phone" placeholder="Nhập Số điện thoại mặc định" class="form-control @error('phone') is-invalid @enderror" value="{{ $editSetting->phone }}">
                    @error('phone')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-address" class=" form-control-label">Địa chỉ</label><span class="text-danger font-weight-bold">*</span>
                    <input type="text" id="nf-address" name="address" placeholder="Nhập địa chỉ" class="form-control @error('address') is-invalid @enderror" value="{{ $editSetting->address }}">
                    @error('address')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-image" class=" form-control-label">Logo</label><span class="text-danger font-weight-bold">*</span>
                    @error('image')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                    <div class="card-box text-center d-flex flex-column align-items-center">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-modal-image">Select image <span class="text-danger">*</span></button>
                            <input type="hidden" name="image" id="image" value="{{ $editSetting->image }}">
                            
                            @error('image')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                        </span>
                        <div id="preview_image" class="mt-3 preview_image">
                            <div class="box_imgg position-relative">
                                <img src="{!! str_replace('/source/','/thumbs/', $editSetting->image) !!}" id='show-img-image' class="show-img-image" style="width:100%;">
                                <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="nf-title" class=" form-control-label">Title</label>
                    <input type="text" id="nf-title" name="title" placeholder="Title" class="form-control @error('title') is-invalid @enderror" value="{{ $editSetting->title }}">
                    @error('title')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-description" class=" form-control-label">Description</label>
                    <textarea name="description" id="nf-description" rows="3" placeholder="Description" class="form-control @error('description') is-invalid @enderror">{{$editSetting->description }}</textarea>
                    @error('description')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nf-keyword" class=" form-control-label">Keyword</label>
                    <textarea name="keyword" id="nf-keyword" rows="3" placeholder="Keyword" class="form-control @error('keyword') is-invalid @enderror">{{$editSetting->keyword }}</textarea>
                    @error('keyword')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Lưu lại
        </button>
        <button type="reset" class="btn btn-danger btn-sm">
            <i class="fa fa-ban"></i> Reset
        </button>
    </div>
</form>
</div>
<div class="modal fade bd-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Manger Image</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0"
                    src="{{ url('/') }}/jadmin/responsive_filemanager/filemanager/dialog.php?type=0&field_id=image">
                </iframe>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')

 <script>
        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#image').val('');
            });
        });
        function responsive_filemanager_callback(field_id){
            var _img = $('input#' + field_id).val();
            if (!_img.length) {
                $('#preview_image').empty();
            } else {
                $('#preview_image').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-image' class="show-img-image" style="width:100%;">
                        <i class="fa fa-times-circle  style_icon_remove_image" title="delete"></i>
                    </div>
                `;
                $('#preview_image').append($html);
                let __img = _img.replace("/source/", "/thumbs/");
                $('#show-img-image').attr('src', __img);
            }
        }
 </script>
@endpush