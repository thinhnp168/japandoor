<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home')->name('home');
Route::get('/{slug?}-dmtt{id}.html', 'PostController@postCategoryPost')->name('view_postCategoryPost')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/{slug?}-cttt{id}.html', 'PostController@postDetail')->name('view_postDetail')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/{slug?}-dmdl{id}.html', 'AgencyController@listAgencyInProvince')->name('view_listAgency_in_province')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/{slug?}-ctdl{id}.html', 'AgencyController@detailAgency')->name('view_detailAgency')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/{slug?}-dmsp{id}.html', 'ProductController@listProduct')->name('view_listProduct')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/{slug?}-ctsp{id}.html', 'ProductController@detailProduct')->name('view_detailProduct')->where('slug', '[a-zA-Z0-9-_]+')->where('id', '[0-9]+');
Route::get('/dat-hang.html', 'HomeController@formOrder')->name('view_formOrder');
Route::post('/dat-hang.html', 'HomeController@order')->name('view_order');
Route::get('/dai-ly.html', 'AgencyController@listAgency')->name('view_listAgency');
Route::get('/san-pham.html', 'ProductController@allProduct')->name('view_allProduct');
Route::get('/dang-nhap.html', 'HomeController@Login')->name('login');
Route::get('/lien-he.html', 'HomeController@contact')->name('contact');
Route::post('/lien-he.html', 'HomeController@saveContact')->name('saveContact');
Route::get('/dang-xuat.html', 'HomeController@Logout')->name('logout');
Route::post('/dang-nhap.html', 'HomeController@PostLogin')->name('postlogin');
Route::post('/save-email.html', 'HomeController@saveEmail')->name('saveEmail');
Route::post('/tim-cua.html', 'HomeController@searchProduct')->name('searchProduct');
Route::get('/render-sitemap.html', 'HomeController@sitemap')->name('render_sitemap');



Route::group(['namespace' => 'admin', 'prefix' => 'admin', 'middleware' => 'checklogin'], function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('admin_home');
    Route::prefix('users')->group(function () {
        Route::get('/list', 'UserController@listUser')->name('listUsers');
        Route::get('/add', 'UserController@formAddUser')->name('formAddUser');
        Route::post('/add', 'UserController@addUser')->name('addUser');
        Route::post('/change-info-user/{id?}', 'UserController@changeInfoUser')->name('changeInfoUser');
        Route::post('/change-pass-user/{id?}', 'UserController@changePassUser')->name('changePassUser');
        Route::get('/edit/{id}', 'UserController@formEditUser')->name('formEditUser');
        Route::get('/deleteUser/{id}', 'UserController@deleteUser')->name('deleteUser');
        Route::post('/edit/{id}', 'UserController@editUser')->name('editUser');
    });
    Route::prefix('category_post')->group(function () {
        Route::get('/list', 'CategoryPostController@listCategoryPost')->name('listCategoryPost');
        Route::get('/add', 'CategoryPostController@formAddCategoryPost')->name('formAddCategoryPost');
        Route::get('/delete/{id}', 'CategoryPostController@deleteCategoryPost')->name('deleteCategoryPost');
        Route::get('/edit/{id}', 'CategoryPostController@formEditCategoryPost')->name('formEditCategoryPost');
        Route::post('/add', 'CategoryPostController@AddCategoryPost')->name('AddCategoryPost');
        Route::post('/edit/{id}', 'CategoryPostController@EditCategoryPost')->name('EditCategoryPost');
        Route::post('/change-info-categorypost/{id?}', 'CategoryPostController@changeInfoCategoryPost')->name('changeInfoCategoryPost');
    });
    Route::prefix('post')->group(function () {
        Route::get('/list', 'PostController@listPost')->name('listPost');
        Route::get('/add', 'PostController@formAddPost')->name('formAddPost');
        Route::get('/edit/{id}', 'PostController@formEditPost')->name('formEditPost');
        Route::post('/edit/{id}', 'PostController@EditPost')->name('EditPost');
        Route::post('/add', 'PostController@addPost')->name('addPost');
        Route::post('/change-info-post/{id?}', 'PostController@changeInfoPost')->name('changeInfoPost');
        Route::post('/change-arrange-post/{id?}', 'PostController@changeArrangePost')->name('changeArrangePost');
        Route::get('/delete/{id}', 'PostController@deletePost')->name('deletePost');
    });
    Route::prefix('color')->group(function () {
        Route::get('/list', 'ColorController@listColor')->name('listColor');
        Route::get('/add', 'ColorController@formAddColor')->name('formAddColor');
        Route::post('/add', 'ColorController@addColor')->name('addColor');
        Route::get('/delete/{id}', 'ColorController@deleteColor')->name('deleteColor');
        Route::post('/change-info-color/{id?}', 'ColorController@changeInfoColor')->name('changeInfoColor');
        Route::post('/change-name-color/{id?}', 'ColorController@changeNameColor')->name('changeNameColor');
    });
    Route::prefix('accessory')->group(function () {
        Route::get('/list', 'AccessoryController@listAccessory')->name('listAccessory');
        Route::get('/add', 'AccessoryController@formAddAccessory')->name('formAddAccessory');
        Route::get('/edit/{id}', 'AccessoryController@formEditAccessory')->name('formEditAccessory');
        Route::get('/delete/{id}', 'AccessoryController@deleteAccessory')->name('deleteAccessory');
        Route::post('/edit/{id}', 'AccessoryController@editAccessory')->name('editAccessory');
        Route::post('/changeInfoAccessory/{id?}', 'AccessoryController@changeInfoAccessory')->name('changeInfoAccessory');
        Route::post('/add', 'AccessoryController@addAccessory')->name('AddAccessory');
    });
    Route::prefix('slider')->group(function () {
        Route::get('/list', 'SliderController@listSlider')->name('listSlider');
        Route::get('/add', 'SliderController@formAddSlider')->name('formAddSlider');
        Route::get('/edit/{id}', 'SliderController@formEditSlider')->name('formEditSlider');
        Route::get('/delete/{id}', 'SliderController@deleteSlider')->name('deleteSlider');
        Route::post('/edit/{id}', 'SliderController@editSlider')->name('editSlider');
        Route::post('/add', 'SliderController@addSlider')->name('addSlider');
        Route::post('/changeInfoSlider/{id?}', 'SliderController@changeInfoSlider')->name('changeInfoSlider');
        Route::post('/changeValueSlider/{id?}', 'SliderController@changeValueSlider')->name('changeValueSlider');
    });
    Route::prefix('agencies')->group(function () {
        Route::get('/list-province', 'AgencyController@listProvince')->name('listProvince');
        Route::get('/list-agency', 'AgencyController@listAgency')->name('listAgency');
        Route::get('/add-province', 'AgencyController@formAddProvince')->name('formAddProvince');
        Route::get('/add-agency', 'AgencyController@formAddAgency')->name('formAddAgency');
        Route::post('/add-agency', 'AgencyController@addAgency')->name('addAgency');
        Route::get('/delete-province/{id}', 'AgencyController@deleteProvince')->name('deleteProvince');
        Route::post('/add-province', 'AgencyController@addProvince')->name('addProvince');
        Route::post('/change-value-province/{id?}', 'AgencyController@changeValueProvince')->name('changeValueProvince');
        Route::get('/delete-agency/{id}', 'AgencyController@formDeleteAgency')->name('formDeleteAgency');
        Route::get('/edit-agency/{id}', 'AgencyController@formEditAgency')->name('formEditAgency');
        Route::post('/edit-agency/{id}', 'AgencyController@editAgency')->name('editAgency');
    });
    Route::prefix('category')->group(function () {
        Route::get('/list', 'CategoryController@listCategory')->name('listCategory');
        Route::get('/delete/{id}', 'CategoryController@deleteCategory')->name('deleteCategory');
        Route::get('/edit/{id}', 'CategoryController@formEditCategory')->name('formEditCategory');
        Route::post('/edit/{id}', 'CategoryController@editCategory')->name('editCategory');
        Route::get('/add', 'CategoryController@formAddCategory')->name('formAddCategory');
        Route::post('/add', 'CategoryController@addCategory')->name('addCategory');
        Route::post('/changeInfoCategory/{id?}', 'CategoryController@changeInfoCategory')->name('changeInfoCategory');
        Route::post('/changeValueCategory/{id?}', 'CategoryController@changeValueCategory')->name('changeValueCategory');
    });
    Route::prefix('product')->group(function () {
        Route::get('/list', 'ProductController@listProduct')->name('listProduct');
        Route::get('/delete/{id}', 'ProductController@deleteProduct')->name('deleteProduct');
        Route::post('/edit/{id}', 'ProductController@editProduct')->name('editProduct');
        Route::get('/edit/{id}', 'ProductController@formEditProduct')->name('formEditProduct');
        Route::get('/add', 'ProductController@formAddProduct')->name('formAddProduct');
        Route::post('/add', 'ProductController@addProduct')->name('addProduct');
        Route::post('/changeInfoProduct/{id?}', 'ProductController@changeInfoProduct')->name('changeInfoProduct');
        Route::post('/changeValueProduct/{id?}', 'ProductController@changeValueProduct')->name('changeValueProduct');
    });
    Route::prefix('setting')->group(function () {
        Route::get('/', 'SettingController@formEditSetting')->name('formEditSetting');
        Route::post('/', 'SettingController@editSetting')->name('editSetting');
    });
    Route::prefix('contact')->group(function () {
        Route::get('/list', 'ContactController@listContact')->name('listContact');
    });
    Route::prefix('email')->group(function () {
        Route::get('/list', 'EmailController@listEmail')->name('listEmail');
    });
    Route::prefix('order')->group(function () {
        Route::get('/list', 'OrderController@listOrder')->name('listOrder');
    });
});